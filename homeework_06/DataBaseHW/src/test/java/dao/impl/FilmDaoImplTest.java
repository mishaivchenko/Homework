package dao.impl;

import Util.DbUtil;
import dao.ActorDao;
import dao.FilmDao;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import pojo.Actor;
import pojo.Film;

import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class FilmDaoImplTest {
    private static final String DELETE_ALL ="delete from films";
    Connection connection;
    DbUtil dbUtil = new DbUtil();
    PreparedStatement preparedStatement;
    Statement statement;

    @After
    public void deleteAll() throws SQLException {
        try {
            connection = dbUtil.getConnection();
            preparedStatement = connection.prepareStatement(DELETE_ALL);
            preparedStatement.executeUpdate();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if(preparedStatement!=null) {
                preparedStatement.close();
            }
            if (connection!=null) {
                connection.close();
            }
        }

    }


    @Before
    public void setUp() throws Exception {
        FilmDao filmDao = new FilmDaoImpl();
        List<Film> filmList = new ArrayList<>();
        filmList.add(new Film(1,"The Lord of the Rings 1",2000, 12000000L));
        filmList.add(new Film(2,"The Lord of the Rings 2",2001, 12000000L));
        filmList.add(new Film(3,"The Lord of the Rings 3",2002, 12000000L));
        filmDao.add(filmList);

    }

    @Test
    public void findAllMustReturnEqualListToExpected() {
        //Given
        FilmDao filmDao = new FilmDaoImpl();
        List<Film> expectedFilmList = new ArrayList<>();
        expectedFilmList.add(new Film(1,"The Lord of the Rings 1",2000, 12000000L));
        expectedFilmList.add(new Film(2,"The Lord of the Rings 2",2001, 12000000L));
        expectedFilmList.add(new Film(3,"The Lord of the Rings 3",2002, 12000000L));
        //When
        List<Film> filmList = filmDao.findAll();
        //Then
        for (int i = 0;i<filmList.size();i++){
            Assert.assertEquals(expectedFilmList.get(i),filmList.get(i));
        }
    }

    @Test
    public void findByIdMustReturnsTheLordOfTheRingsPartOne() throws FileNotFoundException, SQLException {
        //Given
        FilmDao filmDao = new FilmDaoImpl();
        Film expectedFilm = new Film(1,"The Lord of the Rings 1",2000, 12000000L);
        //When
        Film film = filmDao.findById(1);
        //Then
        Assert.assertEquals(expectedFilm,film);
    }

    @Test
    public void addTestAfterAddValueMustBeEqualHobbit() throws SQLException, FileNotFoundException {
        //Given
        FilmDao filmDao = new FilmDaoImpl();
        Film expectedFilm = new Film(4,"Hobbit",2012, 12000000L);
        //When
        filmDao.add(expectedFilm);
        //Then
        Assert.assertEquals(expectedFilm,filmDao.findById(4));
    }

    @Test
    public void addListTestAfterAddTableMustHaveThreePartsOfHobbitAndLOR() throws SQLException {
        //Given
        FilmDao filmDao = new FilmDaoImpl();
        List<Film> expectedFilmList = new ArrayList();
        List<Film> filmsToAdd= new ArrayList<>();
        expectedFilmList.add(new Film(1,"The Lord of the Rings 1",2000, 12000000L));
        expectedFilmList.add(new Film(2,"The Lord of the Rings 2",2001, 12000000L));
        expectedFilmList.add(new Film(3,"The Lord of the Rings 3",2002, 12000000L));
        expectedFilmList.add(new Film(4,"Hobbit 1",2012,1220000l));
        expectedFilmList.add(new Film(5,"Hobbit 2",2013,1220000l));
        expectedFilmList.add(new Film(6,"Hobbit 3",2014,1220000l));

        //When

        List<Film> listAfterAdd = filmDao.findAll();
        filmsToAdd.add(new Film(4,"Hobbit 1",2012,1220000l));
        filmsToAdd.add(new Film(5,"Hobbit 2",2013,1220000l));
        filmsToAdd.add(new Film(6,"Hobbit 3",2014,1220000l));
        filmDao.add(filmsToAdd);
        //Then
        for (int i = 0;i<listAfterAdd.size();i++){
            Assert.assertEquals(expectedFilmList.get(i),listAfterAdd.get(i));
        }
    }

    @Test
    public void updateTestAfterUpdateFirstElementMustBeEqualHobbit() throws SQLException, FileNotFoundException {
        //Given
        FilmDao filmDao = new FilmDaoImpl();
        Film expectedFilm = new Film(1,"Hobbit",2012, 12000000L);
        //When
        filmDao.update(expectedFilm);
        //Then
        Assert.assertEquals(expectedFilm,filmDao.findById(1));
    }

    @Test
    public void updateListAfterUpdateLorMustBeChangeOnHobbit() throws SQLException {
        //Given
        FilmDao filmDao = new FilmDaoImpl();
        List<Film> expectedList= new ArrayList<>();
        expectedList.add(new Film(1,"Hobbit 1",2012,1220000l));
        expectedList.add(new Film(2,"Hobbit 2",2013,1220000l));
        expectedList.add(new Film(3,"Hobbit 3",2014,1220000l));
        //When
        filmDao.update(expectedList);
        List<Film> listAfterUpdate = filmDao.findAll();
        //Then
        for (int i = 0;i<listAfterUpdate.size();i++){
            Assert.assertEquals(expectedList.get(i),listAfterUpdate.get(i));
        }
    }

    @Test
    public void TestAfterDeleteListMustBeNotContainsLOR3() throws SQLException {
        //Given
        FilmDao filmDao = new FilmDaoImpl();
        List<Film> expectedFilmList = new ArrayList();
        List<Film> filmsAfterDelete= new ArrayList<>();
        expectedFilmList.add(new Film(1,"The Lord of the Rings 1",2000, 12000000L));
        expectedFilmList.add(new Film(2,"The Lord of the Rings 2",2001, 12000000L));
        //When
        filmDao.delete(new Film(3,"The Lord of The Rings 3",2003,1220000l));

        filmsAfterDelete = filmDao.findAll();
        //Then
        for (int i = 0;i<filmsAfterDelete.size();i++){
            Assert.assertEquals(expectedFilmList.get(i),filmsAfterDelete.get(i));
        }
    }

    @Test
    public void  deleteListTestAfterDeleteListMustBeEmpty() throws SQLException {
        //Given
        FilmDao filmDao = new FilmDaoImpl();
        List<Film> expectedFilmList = new ArrayList();
        List<Film> filmListToDelete = new ArrayList<>();
        filmListToDelete.add(new Film(1,"The Lord of the Rings 1",2000, 12000000L));
        filmListToDelete.add(new Film(2,"The Lord of the Rings 2",2001, 12000000L));
        filmListToDelete.add(new Film(3,"The Lord of the Rings 3",2002, 12000000L));
        //When
        filmDao.delete(filmListToDelete);
        List<Film> ListAfterDelete = filmDao.findAll();
        //Then
        Assert.assertEquals(expectedFilmList,ListAfterDelete);

    }
}
