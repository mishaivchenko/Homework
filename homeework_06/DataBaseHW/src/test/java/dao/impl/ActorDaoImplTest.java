package dao.impl;

import Util.DbUtil;
import dao.ActorDao;
import dao.FilmDao;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import pojo.Actor;
import pojo.Film;

import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ActorDaoImplTest {
    private static final String DELETE_ALL ="delete from actors";
    Connection connection;
    DbUtil dbUtil = new DbUtil();
    PreparedStatement preparedStatement;
    Statement statement;

    @Before
    public void setUp() throws Exception {

        ActorDao actorDao = new ActorDaoImpl();
        List<Actor> actorList = new ArrayList<>();
        actorList.add(new Actor(1,"Vigo Mortensen"));
        actorList.add(new Actor(2,"Elaigio Vood"));
        actorList.add(new Actor(3,"Ien Makmillen"));
        actorList.add(new Actor(4,"Liv Tayler"));
        actorDao.add(actorList);
    }
    @After
    public void deleteAll() throws SQLException {
        try {
            connection = dbUtil.getConnection();
            preparedStatement = connection.prepareStatement(DELETE_ALL);
            preparedStatement.executeUpdate();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if(preparedStatement!=null) {
                preparedStatement.close();
            }
            if (connection!=null) {
                connection.close();
            }
        }

    }


    @Test
    public void findAllMustReturnEqualListToExpected() throws SQLException {
        //Given
        ActorDao actorDao = new ActorDaoImpl();
        List<Actor> expectedActorList = new ArrayList<>();
        expectedActorList.add(new Actor(1,"Vigo Mortensen"));
        expectedActorList.add(new Actor(2,"Elaigio Vood"));
        expectedActorList.add(new Actor(3,"Ien Makmillen"));
        expectedActorList.add(new Actor(4,"Liv Tayler"));
        //When
        List<Actor> list = actorDao.findAll();
        //Then
        for (int i = 0;i<list.size();i++){
            Assert.assertEquals(expectedActorList.get(i),list.get(i));
        }
    }

    @Test
    public void findByIdMustReturnVigoMortensen() throws FileNotFoundException, SQLException, ClassNotFoundException {
        //Given
        ActorDao actorDao = new ActorDaoImpl();
        Actor expectedActor = new Actor(1,"Vigo Mortensen");
        //When
        Actor actorFromDb = actorDao.findById(1);
        //Then
        Assert.assertEquals(expectedActor,actorFromDb);
    }

    @Test
    public void addTestAfterAddListMustContainsOrlandoBlum() throws SQLException {
        //Given
        Actor actorToAdd = new Actor(5,"Orlando Blum");
        ActorDao actorDao = new ActorDaoImpl();
        List<Actor> expectedActorList = new ArrayList<>();
        expectedActorList.add(new Actor(1,"Vigo Mortensen"));
        expectedActorList.add(new Actor(2,"Elaigio Vood"));
        expectedActorList.add(new Actor(3,"Ien Makmillen"));
        expectedActorList.add(new Actor(4,"Liv Tayler"));
        //When
        actorDao.add(actorToAdd);
        expectedActorList.add(actorToAdd);
        List<Actor> listAfterAdd = actorDao.findAll();
        //Then
        for (int i = 0;i<listAfterAdd.size();i++){
            Assert.assertEquals(expectedActorList.get(i),listAfterAdd.get(i));
        }
    }

    @Test
    public void addListTestAfterAddMustContainsThreeNewActors() throws SQLException {
        //Given
        ActorDao actorDao = new ActorDaoImpl();
        List<Actor> expectedActorList = new ArrayList<>();
        expectedActorList.add(new Actor(1,"Vigo Mortensen"));
        expectedActorList.add(new Actor(2,"Elaigio Vood"));
        expectedActorList.add(new Actor(3,"Ien Makmillen"));
        expectedActorList.add(new Actor(4,"Liv Tayler"));
        Actor Gimli = new Actor(5,"Shon Riz-Devis");
        Actor Saruman = new Actor(6,"Kristofer Li");
        Actor Tyk = new Actor(7,"Billi Boyd");

         List<Actor> listToAdd = new ArrayList<>();
        listToAdd.add(Gimli);
        listToAdd.add(Saruman);
        listToAdd.add(Tyk);
        //When
        actorDao.add(listToAdd);
        expectedActorList.add(Gimli);
        expectedActorList.add(Saruman);
        expectedActorList.add(Tyk);
        List<Actor> listAfterAdd = actorDao.findAll();
        //Then
        for (int i = 0;i<listAfterAdd.size();i++){
            Assert.assertEquals(expectedActorList.get(i),listAfterAdd.get(i));
        }
    }

    @Test
    public void updateTestAfterUpdateSecondElementMustBeEqualKristoferLi() throws SQLException {
        //Given
        ActorDao actorDao = new ActorDaoImpl();
        List<Actor> expectedActorList = new ArrayList<>();
        expectedActorList.add(new Actor(1,"Vigo Mortensen"));
        expectedActorList.add(new Actor(2,"Kristofer Li"));
        expectedActorList.add(new Actor(3,"Ien Makmillen"));
        expectedActorList.add(new Actor(4,"Liv Tayler"));
        //When
        actorDao.update(new Actor(2,"Kristofer Li"));
        List<Actor> listAfterUpdate = actorDao.findAll();
        //Then
        for (int i = 0;i<listAfterUpdate.size();i++){
        Assert.assertEquals(expectedActorList.get(i),listAfterUpdate.get(i));
    }
}

    @Test
    public void updateListTestAfterUpdateFirstSecondThirdPositionOnTheListMustBeChanged() throws SQLException {
        //Given
        ActorDao actorDao = new ActorDaoImpl();
        List<Actor> expectedActorList = new ArrayList<>();
        expectedActorList.add(new Actor(1,"Shon Riz-Devis"));
        expectedActorList.add(new Actor(2,"Kristofer Li"));
        expectedActorList.add(new Actor(3,"Billi Boyd"));
        expectedActorList.add(new Actor(4,"Liv Tayler"));

        Actor Gimli = new Actor(1,"Shon Riz-Devis");
        Actor Saruman = new Actor(2,"Kristofer Li");
        Actor Tyk = new Actor(3,"Billi Boyd");
        List<Actor> listToUpdate = new ArrayList<>();
        listToUpdate.add(Gimli);
        listToUpdate.add(Saruman);
        listToUpdate.add(Tyk);
        //When
        actorDao.update(listToUpdate);
        List<Actor> listAfterUpdate = actorDao.findAll();

        //Then
        for (int i = 0;i<listAfterUpdate.size();i++){
            Assert.assertEquals(expectedActorList.get(i),listAfterUpdate.get(i));
        }
    }

    @Test
    public void deleteTestAfterDeleteListMustBeNotContainsVigoMortensen() throws SQLException {
        //Given
        ActorDao actorDao = new ActorDaoImpl();
        List<Actor> expectedActorList = new ArrayList<>();
        expectedActorList.add(new Actor(2,"Elaigio Vood"));
        expectedActorList.add(new Actor(3,"Ien Makmillen"));
        expectedActorList.add(new Actor(4,"Liv Tayler"));
        //When
        actorDao.delete(new Actor(1,"Vigo Mortensen"));
        List<Actor> listAfterDelete = actorDao.findAll();
        //Then
        for (int i = 0;i<listAfterDelete.size();i++){
            Assert.assertEquals(expectedActorList.get(i),listAfterDelete.get(i));
        }
    }

    @Test
    public void deleteListTestAfterDeleteListMustBeEmpty() throws SQLException {
        //Given
        ActorDao actorDao = new ActorDaoImpl();
        List<Actor> expectedActorList = new ArrayList<>();
            List<Actor> listToDelete = new ArrayList<>();
        listToDelete.add(new Actor(1,"Vigo Mortensen"));
        listToDelete.add(new Actor(2,"Elaigio Vood"));
        listToDelete.add(new Actor(3,"Ien Makmillen"));
        listToDelete.add(new Actor(4,"Liv Tayler"));
        //When
        actorDao.delete(listToDelete);
        List<Actor> listAfterDelete = actorDao.findAll();
        //Then
        Assert.assertEquals(expectedActorList,listAfterDelete);
    }
}