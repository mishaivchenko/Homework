package Util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;


public class DbUtil {
        private  String username;
        private  String password;
        private  String url;



    public Connection getConnection() throws ClassNotFoundException, SQLException, FileNotFoundException {
        Connection connection = null;
        if (checkingForAPropertiesFile()) {

            Properties properties = initProperties();
            Class.forName(properties.getProperty("driver"));
            this.username = properties.getProperty("username");
            this.password = properties.getProperty("password");
            this.url = properties.getProperty("url");

            connection = DriverManager.getConnection(url,username,password);
            return connection;
        } else throw new ClassNotFoundException();

    }

    private Properties initProperties() throws FileNotFoundException {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(new File("src/main/resources/db.properties")));
            return properties;

        } catch (IOException e) {
            e.printStackTrace();
        }
        throw new FileNotFoundException();
    }

    private boolean checkingForAPropertiesFile(){
        return (new File("src/main/resources/db.properties").exists())? true: false;
    }

}
