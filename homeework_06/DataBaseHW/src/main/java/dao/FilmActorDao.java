package dao;

import pojo.Actor;
import pojo.FilmActor;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.List;

public interface FilmActorDao {
    //Read
    List<FilmActor> findAll() throws SQLException;
    FilmActor findById(int filmId,int actorId) throws FileNotFoundException, SQLException, ClassNotFoundException;
    //Create
    void add(FilmActor filmActor) throws SQLException;
    void add(List<FilmActor> filmActors) throws SQLException;
    //Update
    void update(FilmActor filmActor) throws SQLException;
    void update(List<FilmActor> filmActors) throws SQLException;
    //Delete
    void delete(FilmActor filmActor) throws SQLException;
    void delete(List<FilmActor> filmActors) throws SQLException;
}
