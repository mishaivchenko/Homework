package dao;

import pojo.Actor;
import pojo.Film;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.List;

public interface FilmDao {
    List<Film> findAll();
    Film findById(int id) throws FileNotFoundException, SQLException;

    void add(Film film) throws SQLException;
    void add(List<Film> films) throws SQLException;

    void update(Film film) throws SQLException;
    void update(List<Film> films) throws SQLException;

    void delete(Film film) throws SQLException;
    void delete(List<Film> films) throws SQLException;
}
