package dao;

import pojo.Actor;
import pojo.Film;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.List;

public interface ActorDao {
//Read
     List<Actor> findAll() throws SQLException;
     Actor findById(int id) throws FileNotFoundException, SQLException, ClassNotFoundException;
//Create
     void add(Actor actor) throws SQLException;
     void add(List<Actor> actors) throws SQLException;
//Update
     void update(Actor actor) throws SQLException;
     void update(List<Actor> actors) throws SQLException;
//Delete
     void delete(Actor actor) throws SQLException;
     void delete(List<Actor> actors) throws SQLException;

}
