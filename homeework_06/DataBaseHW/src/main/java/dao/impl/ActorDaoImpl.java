package dao.impl;

import Util.DbUtil;
import dao.ActorDao;
import dao.FilmActorDao;
import pojo.Actor;
import pojo.Film;
import pojo.FilmActor;

import java.io.FileNotFoundException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ActorDaoImpl implements ActorDao {

    private static final String DELETE_ALL ="delete from actors";

    private static final String FIND_ALL ="select * from actors";

    private static final String FIND_BY_ID = "select id, name from actors where id =?";

    private static final String INSERT ="INSERT INTO actors (id, name) VALUES(?,?)";

    private static final String UPDATE = "UPDATE actors SET name = ? WHERE id = ?";

    private static final String DELETE = "DELETE FROM actors WHERE ID=?";


    private DbUtil dbUtil = new DbUtil();
    private PreparedStatement preparedStatement = null;
    private Connection connection;
    private Statement statement = null;


    @Override
    public List<Actor> findAll() throws SQLException {
        List<Actor> actorList = new ArrayList<>();


        try {
            connection = dbUtil.getConnection();
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(FIND_ALL);
            while (resultSet.next()) {
                Actor actor = new Actor();
                actor.setId(resultSet.getInt("id"));
                actor.setName(resultSet.getString("name"));
                actorList.add(actor);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                connection.close();
            }
        }
        return actorList;
    }




    @Override
    public Actor findById(int id) throws FileNotFoundException, SQLException {
        Actor actor = new Actor();
        try {
            connection = dbUtil.getConnection();
            preparedStatement = connection.prepareStatement(FIND_BY_ID);
            preparedStatement.setInt(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();
                if(resultSet.next()) {
                    actor.setId(resultSet.getInt("id"));
                    actor.setName(resultSet.getString("name"));
                }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            closeResources(preparedStatement,connection);
        }
    return actor;
    }



    @Override
    public void add(Actor actor) throws SQLException {
        try {
            connection = dbUtil.getConnection();

            preparedStatement = connection.prepareStatement(INSERT);
                    preparedStatement.setInt(1,actor.getId());
                    preparedStatement.setString(2,actor.getName());

            preparedStatement.executeUpdate();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            closeResources(preparedStatement,connection);
        }
    }

    @Override
    public void add(List<Actor> actors) throws SQLException {
                for (Actor actor:actors) {
                    add(actor);
                }
        }

    @Override
    public void update(Actor actor) throws SQLException {
        try {
            connection = dbUtil.getConnection();
            preparedStatement = connection.prepareStatement(UPDATE);
            preparedStatement.setString(1,actor.getName());
            preparedStatement.setInt(2,actor.getId());
            preparedStatement.executeUpdate();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            closeResources(preparedStatement,connection);
        }
    }

    @Override
    public void update(List<Actor> actors) throws SQLException {
        for (Actor actor:actors) {
            update(actor);
        }
        }


    @Override
    public void delete(Actor actor) throws SQLException {
            try  {
                connection = dbUtil.getConnection();

                preparedStatement = connection.prepareStatement(DELETE);
                preparedStatement.setInt(1, actor.getId());
                preparedStatement.executeUpdate();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } finally {
                closeResources(preparedStatement,connection);
            }
    }
    @Override
    public void delete(List<Actor> actors) throws SQLException {

        for (Actor actor : actors) {
            delete(actor);
        }
    }

    private void closeResources(PreparedStatement preparedStatement, Connection connection) throws SQLException {
        if(preparedStatement!=null) {
            preparedStatement.close();
        }
            if (connection!=null) {
                connection.close();
            }
    }

    public void deleteAll() throws SQLException {
        try {
            connection = dbUtil.getConnection();
            preparedStatement = connection.prepareStatement(DELETE_ALL);
            preparedStatement.executeUpdate();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            closeResources(preparedStatement,connection);
        }

    }

}
