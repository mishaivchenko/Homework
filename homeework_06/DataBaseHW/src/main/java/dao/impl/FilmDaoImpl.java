package dao.impl;

import Util.DbUtil;
import dao.FilmDao;
import pojo.Actor;
import pojo.Film;

import java.io.FileNotFoundException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class FilmDaoImpl implements FilmDao {

    private static final String FIND_ALL ="select * from films";

    private static final String FIND_BY_ID = "select id,title,filmBudget,year  from films where id =?";

    private static final String INSERT ="INSERT INTO films (ID,title,filmBudget,year) VALUES(?,?,?,?)";

    private static final String UPDATE = "UPDATE films SET title=?,filmBudget=?,year=? WHERE id=?";

    private static final String DELETE = "DELETE FROM films WHERE ID=?";

   private DbUtil dbUtil = new DbUtil();

   private PreparedStatement preparedStatement = null;

   private Connection connection;

    private Statement statement = null;


    @Override
    public List<Film> findAll() {
        List<Film> filmList = new ArrayList<>();
        try {
            connection = dbUtil.getConnection();
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(FIND_ALL);
            while (resultSet.next()) {
                Film film = new Film();
                film.setId(resultSet.getInt("id"));
                film.setTitle(resultSet.getString("title"));
                film.setFilmBudget(resultSet.getLong("filmBudget"));
                film.setYear(resultSet.getInt("year"));
                filmList.add(film);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
      return filmList;
    }

    @Override
    public Film findById(int id) throws FileNotFoundException, SQLException {
        Film film = new Film();
        try {
            connection = dbUtil.getConnection();
            preparedStatement = connection.prepareStatement(FIND_BY_ID);
            preparedStatement.setInt(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()) {
                film.setId(resultSet.getInt("id"));
                film.setTitle(resultSet.getString("title"));
                film.setFilmBudget(resultSet.getLong("filmBudget"));
                film.setYear(resultSet.getInt("year"));;
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            closeResources(preparedStatement,connection);
        }
        return film;
    }

    @Override
    public void add(Film film) throws SQLException {
        try {
            connection = dbUtil.getConnection();

            preparedStatement = connection.prepareStatement(INSERT);
            preparedStatement.setInt(1,film.getId());
            preparedStatement.setString(2,film.getTitle());
            preparedStatement.setInt(4,film.getYear());
            preparedStatement.setLong(3,film.getFilmBudget());
            preparedStatement.executeUpdate();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            closeResources(preparedStatement,connection);
        }
    }

    @Override
    public void add(List<Film> films) throws SQLException {
        try {
            connection = dbUtil.getConnection();
            for (Film film:films) {
                preparedStatement = connection.prepareStatement(INSERT);
                preparedStatement.setInt(1, film.getId());
                preparedStatement.setString(2, film.getTitle());
                preparedStatement.setInt(4, film.getYear());
                preparedStatement.setLong(3, film.getFilmBudget());
                preparedStatement.executeUpdate();
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            closeResources(preparedStatement,connection);
        }
    }

    @Override
    public void update(Film film) throws SQLException {
        try {
            connection = dbUtil.getConnection();

            preparedStatement = connection.prepareStatement(UPDATE);
            preparedStatement.setInt(4,film.getId());
            preparedStatement.setString(1,film.getTitle());
            preparedStatement.setInt(3,film.getYear());
            preparedStatement.setLong(2,film.getFilmBudget());
            preparedStatement.executeUpdate();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            closeResources(preparedStatement,connection);
        }
    }

    @Override
    public void update(List<Film> films) throws SQLException {

        try {
            connection = dbUtil.getConnection();
            for (Film film:films) {

            preparedStatement = connection.prepareStatement(UPDATE);
            preparedStatement.setInt(4,film.getId());
            preparedStatement.setString(1,film.getTitle());
            preparedStatement.setInt(3,film.getYear());
            preparedStatement.setLong(2,film.getFilmBudget());
            preparedStatement.executeUpdate();
            }

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            closeResources(preparedStatement,connection);
        }
    }

    @Override
    public void delete(Film film) throws SQLException {
        try  {
            connection = dbUtil.getConnection();
            preparedStatement = connection.prepareStatement(DELETE);
            preparedStatement.setInt(1, film.getId());
            preparedStatement.executeUpdate();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            closeResources(preparedStatement,connection);
        }
    }

    @Override
    public void delete(List<Film> films) throws SQLException {
        try  {
            for (Film film:films) {
                connection = dbUtil.getConnection();
                preparedStatement = connection.prepareStatement(DELETE);
                preparedStatement.setInt(1, film.getId());
                preparedStatement.executeUpdate();
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            closeResources(preparedStatement,connection);
        }
    }

    private void closeResources(PreparedStatement preparedStatement, Connection connection) throws SQLException {
        if(preparedStatement!=null) {
            preparedStatement.close();
        }
        if (connection!=null) {
            connection.close();
        }
    }
}
