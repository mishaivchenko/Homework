package dao.impl;

import Util.DbUtil;
import dao.ActorDao;
import dao.FilmActorDao;
import dao.FilmDao;
import pojo.Actor;
import pojo.Film;
import pojo.FilmActor;

import java.io.FileNotFoundException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class FilmActorDaoImpl implements FilmActorDao {

    private static final String DELETE_ALL = "delete from film_actors";

    private static final String FIND_ALL ="select * from films_actors";

    private static final String FIND_BY_ID = "select film_id, actor_id  from films_actors where film_id =? and actor_id =?";

    private static final String INSERT ="INSERT INTO films_actors (film_id,actor_id) VALUES(?,?)";

    private static final String UPDATE = "UPDATE films_actors SET actor_id = ?, film_id= ? ";

    private static final String DELETE = "DELETE FROM films_actors WHERE film_id=? and actor_id=?";

    private DbUtil dbUtil = new DbUtil();


    private PreparedStatement preparedStatement = null;

    private Connection connection;

    private Statement statement = null;


    @Override
    public List<FilmActor> findAll() throws SQLException {
        List<FilmActor> filmActorList = new ArrayList<>();
        try {
            connection = dbUtil.getConnection();
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(FIND_ALL);
            while (resultSet.next()) {
                FilmActor filmActor= new FilmActor();
                filmActor.setActorId(resultSet.getInt("actor_id"));
                filmActor.setFilmId(resultSet.getInt("film_id"));
                filmActorList.add(filmActor);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return filmActorList ;
    }


    @Override
    public FilmActor findById(int filmId,int actorId) throws FileNotFoundException, SQLException, ClassNotFoundException {
        FilmActor filmActor = new FilmActor();
        try {
            connection = dbUtil.getConnection();
            preparedStatement = connection.prepareStatement(FIND_BY_ID);
            preparedStatement.setInt(1,filmId);
            preparedStatement.setInt(2,actorId);

            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()) {
                filmActor.setActorId(resultSet.getInt("actor_id"));
                filmActor.setFilmId(resultSet.getInt("film_id"));
               }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            closeResources(preparedStatement,connection);
        }
        return filmActor;
    }

    @Override
    public void add(FilmActor filmActor) throws SQLException {
        try {
            connection = dbUtil.getConnection();

            preparedStatement = connection.prepareStatement(INSERT);
            preparedStatement.setInt(2,filmActor.getActorId());
            preparedStatement.setInt(1,filmActor.getFilmId());

            preparedStatement.executeUpdate();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            closeResources(preparedStatement,connection);
        }
    }

    @Override
    public void add(List<FilmActor> filmActors) throws SQLException {

            for (FilmActor filmActor:filmActors) {
                    add(filmActor);
            }
    }

    @Override
    public void update(FilmActor filmActor) throws SQLException {
        try {
            connection = dbUtil.getConnection();
                preparedStatement = connection.prepareStatement(UPDATE);
                preparedStatement.setInt(2,filmActor.getFilmId());
                preparedStatement.setInt(1,filmActor.getActorId());
            preparedStatement.executeUpdate();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            closeResources(preparedStatement,connection);
        }
    }

    @Override
    public void update(List<FilmActor> filmActors) throws SQLException {
        for (FilmActor filmActor:filmActors) {
            update(filmActor);
        }
    }

    @Override
    public void delete(FilmActor filmActor) throws SQLException {
        try  {
            connection = dbUtil.getConnection();
            preparedStatement = connection.prepareStatement(DELETE);
            preparedStatement.setInt(1, filmActor.getFilmId());
            preparedStatement.setInt(2,filmActor.getActorId());
            preparedStatement.executeUpdate();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            closeResources(preparedStatement,connection);
        }
    }

    @Override
    public void delete(List<FilmActor> filmActors) throws SQLException {
        for (FilmActor filmActor:filmActors) {
            delete(filmActor);
        }


    }
    private void closeResources(PreparedStatement preparedStatement, Connection connection) throws SQLException {
        if(preparedStatement!=null) {
        preparedStatement.close();
    }
        if (connection!=null) {
        connection.close();
    }
    }
}
