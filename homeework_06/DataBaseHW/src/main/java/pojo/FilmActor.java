package pojo;

import java.util.Objects;

public class FilmActor {
    private int filmId;
    private int actorId;

    public FilmActor(int filmId, int actorId) {
        this.filmId = filmId;
        this.actorId = actorId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FilmActor filmActor = (FilmActor) o;

        if (filmId != filmActor.filmId) return false;
        return actorId == filmActor.actorId;
    }

    @Override
    public int hashCode() {
        int result = filmId;
        result = 31 * result + actorId;
        return result;
    }

    @Override
    public String toString() {
        return "FilmActor{" +
                "filmId=" + filmId +
                ", actorId=" + actorId +
                '}';
    }

    public int getFilmId() {
        return filmId;
    }

    public void setFilmId(int filmId) {
        this.filmId = filmId;
    }

    public int getActorId() {
        return actorId;
    }

    public void setActorId(int actorId) {
        this.actorId = actorId;
    }

    public FilmActor() {

    }

}
