package pojo;

import java.util.List;
import java.util.Objects;

public class Film {

    private int id;
    private String title;
    private int year;
    private Long filmBudget;

    public Film() {
    }

    public Film(int id, String title, int year, Long filmBudget) {
        this.id = id;
        this.title = title;
        this.year = year;
        this.filmBudget = filmBudget;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Long getFilmBudget() {
        return filmBudget;
    }

    public void setFilmBudget(Long filmBudget) {
        this.filmBudget = filmBudget;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Film film = (Film) o;

        if (id != film.id) return false;
        if (year != film.year) return false;
        if (title != null ? !title.equals(film.title) : film.title != null) return false;
        return filmBudget != null ? filmBudget.equals(film.filmBudget) : film.filmBudget == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + year;
        result = 31 * result + (filmBudget != null ? filmBudget.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Film{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", year=" + year +
                ", filmBudget=" + filmBudget +
                '}';
    }
}

