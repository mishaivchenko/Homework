package taskOneDeadLock;

import taskOneDeadLock.pojo.LittleGirl;
import taskOneDeadLock.pojo.ScaryMonster;

public class DeadLockTest implements Runnable{
    LittleGirl littleGirl = new LittleGirl("Бу");
    ScaryMonster monster = new ScaryMonster("Салли");
    DeadLockTest(){
        Thread.currentThread().setName("MainThred");
        Thread thread = new Thread(this,"RacingThred");
        thread.start();
        littleGirl.getScared(monster);
    }

    public static void main(String[] args) {

    new DeadLockTest();



    }

    @Override
    public void run() {
    monster.getSatisfaction(littleGirl);
    }
}
