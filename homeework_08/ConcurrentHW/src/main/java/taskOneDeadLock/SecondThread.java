package taskOneDeadLock;

import taskOneDeadLock.pojo.LittleGirl;
import taskOneDeadLock.pojo.ScaryMonster;

public class SecondThread extends Thread {
    @Override
    public void run() {
        LittleGirl Bu = new LittleGirl("Бу");
        ScaryMonster monster = new ScaryMonster("Sally");
        Bu.getScared(monster);
        try {

            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }



}
