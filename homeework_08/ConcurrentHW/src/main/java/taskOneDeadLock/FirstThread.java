package taskOneDeadLock;

import taskOneDeadLock.pojo.LittleGirl;
import taskOneDeadLock.pojo.ScaryMonster;

public class FirstThread extends Thread {


    @Override
    public void run() {
        LittleGirl Bu = new LittleGirl("Бу");
        ScaryMonster monster = new ScaryMonster("Sally");
        monster.getSatisfaction(Bu);
        try {

            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
