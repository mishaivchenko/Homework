package taskOneDeadLock.pojo;

public class LittleGirl {
    ScaryMonster monster;
    String name;
    boolean scared = false;

    public LittleGirl(String name) {
        this.name = name;
    }

    public synchronized void getScared(ScaryMonster monster){
        try {
            Thread.sleep(1000);
        }  catch (InterruptedException e) {
            e.printStackTrace();
        }
        scared = true;
        System.out.println("Ohhh i'm so scared");
        monster.getSatisfaction(this);
    }
}
