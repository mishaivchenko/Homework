package taskOneDeadLock.pojo;

public class ScaryMonster {
    LittleGirl littleGirl;
    String name;
    boolean satisfaction = false;

    public ScaryMonster(String name) {
        this.name = name;
    }

    public synchronized void getSatisfaction(LittleGirl littleGirl){
        try {
            Thread.sleep(1000);
        }  catch (InterruptedException e) {
            e.printStackTrace();
        }
         this.satisfaction = true;
         System.out.println(name + " finally happy");
        littleGirl.getScared(this);
     }
}
