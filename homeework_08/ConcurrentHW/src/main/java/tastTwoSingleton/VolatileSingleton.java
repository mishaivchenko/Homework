package tastTwoSingleton;

import java.util.List;

public class VolatileSingleton {
    public static void main(String[] args) {

    }
    private static VolatileSingleton singletonInstance = null;
    private volatile static boolean isInstanceCreated = false;
    private List<Integer> list;
        private VolatileSingleton(){
            list.add(1);
            list.add(2);
            list.add(3);
            list.add(4);
            list.add(5);
            list.add(6);
            list.add(7);
            list.add(8);
            list.add(9);
        }
            public static VolatileSingleton getSingletonInstance(){
                if(!isInstanceCreated){
                    synchronized (VolatileSingleton.class){
                        try {
                            if(!isInstanceCreated){
                                singletonInstance = new VolatileSingleton();
                                isInstanceCreated = true;
                            }

                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
                return singletonInstance;
            }
}
