package taskThreeQueue;

import java.util.concurrent.locks.ReentrantLock;

public class Main {


    public static void main(String[] args) throws InterruptedException {
        ReentrantLock lock = new ReentrantLock();
        MyQueueWithLock queue = new MyQueueWithLock(lock);

        Consumer consumer = new Consumer(queue);
        new Producer(queue);
        consumer.start();
        Thread.sleep(500);
        consumer.interrupt();
    }
}
