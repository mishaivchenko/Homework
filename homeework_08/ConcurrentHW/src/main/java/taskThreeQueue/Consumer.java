package taskThreeQueue;



class Consumer extends Thread {
        private MyQueueWithLock queue;

        public Consumer(MyQueueWithLock queue) {
            this.queue = queue;
            this.setName("Consumer");
        }

        @Override
        public void run() {
            while (true) {
                queue.get();
                if (isInterrupted()) {
                    return;
                }
            }
        }
    }

