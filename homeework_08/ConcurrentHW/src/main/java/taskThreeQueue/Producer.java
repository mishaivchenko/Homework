package taskThreeQueue;

public class Producer implements Runnable {

        private MyQueueWithLock queue;

        public Producer(MyQueueWithLock queue) {
            this.queue = queue;
            new Thread(this, "Producer").start();

        }
        @Override
        public void run() {
            for (int i = 1; i <= 5; i++) {
                queue.put(i);
            }
        }
    }

