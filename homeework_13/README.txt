
this program illustrates the operation of the REST server for the essence of "damages" from the pet project.
For the initial migration of the database:
 mvn liquibase: update

The server is tested using Postman.
Link to the collection of requests : https://www.getpostman.com/collections/efb33dcd06b21c0dd8cb