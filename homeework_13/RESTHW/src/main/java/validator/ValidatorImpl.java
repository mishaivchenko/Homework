package validator;

import entity.Damage;

public class ValidatorImpl implements Validator<Damage> {

    @Override
    public boolean validate(Damage entity) {
        if(entity!=null){
            if(entity.getId()>0&&entity.getSum()>=0)return true;
        }
        return false;
    }
}
