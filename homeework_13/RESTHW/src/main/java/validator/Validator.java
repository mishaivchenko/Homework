package validator;

public interface Validator<E> {
    boolean validate(E entity);
}
