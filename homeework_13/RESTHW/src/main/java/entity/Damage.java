package entity;

public class Damage {

    private int id;
    private String description;
    private long sum;
    private boolean isPaid;



    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (int) (sum ^ (sum >>> 32));
        result = 31 * result + (isPaid ? 1 : 0);
        return result;
    }

    public Damage(int id, String description, long sum, boolean isPaid) {
        this.id = id;
        this.description = description;
        this.sum = sum;
        this.isPaid = isPaid;
    }
    public Damage(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getSum() {
        return sum;
    }

    public void setSum(long sum) {
        this.sum = sum;
    }

    public boolean isPaid() {
        return isPaid;
    }

    public void setPaid(boolean paid) {
        isPaid = paid;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Damage damage = (Damage) o;

        if (id != damage.id) return false;
        if (sum != damage.sum) return false;
        if (isPaid != damage.isPaid) return false;
        return description != null ? description.equals(damage.description) : damage.description == null;
    }

    @Override
    public String toString() {
        return "Damage{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", sum=" + sum +
                ", isPaid=" + isPaid +
                '}';
    }
}
