package service;

import service.impl.DamageServiceImpl;

public interface ServiceFactory<Service> {

    DamageServiceImpl getDamageService();

}
