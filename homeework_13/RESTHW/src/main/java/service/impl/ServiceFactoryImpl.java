package service.impl;


import entity.Damage;
import service.Service;
import service.ServiceFactory;

public class ServiceFactoryImpl implements ServiceFactory<Service> {
    private static final ServiceFactoryImpl serviceFactory = new ServiceFactoryImpl();

    private Service<Damage> damageService = new DamageServiceImpl();




    public static ServiceFactoryImpl getServiceFactory(){
        return serviceFactory;
    }

   @Override
    public DamageServiceImpl getDamageService() {
        return (DamageServiceImpl) this.damageService;
    }


}
