package dao;

import config.PropertiesManager;
import org.h2.tools.RunScript;
import org.h2.tools.Server;
import org.omg.CORBA.portable.ApplicationException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBManager {
    private static final String SCHEMA_SCRIPT = "schema.sql";
    private static final String DATA_SCRIPT = "data.sql";
    private static Server server;

    public static void prepareDb(Properties properties) {
        try {
            server = Server.createTcpServer("-tcpAllowOthers").start();
            Class.forName("org.h2.Driver");
            Connection connection = getConnection(properties);
            RunScript.execute(connection, PropertiesManager.loadScript(SCHEMA_SCRIPT));
            RunScript.execute(connection, PropertiesManager.loadScript(DATA_SCRIPT));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void stopDb() {
        if (server != null) {
            server.stop();
        }
    }

    public static Connection getConnection(Properties properties) throws ClassNotFoundException {
        try {
            String url = properties.getProperty("url");
            String username = properties.getProperty("username");
            String password = properties.getProperty("password");
            Class.forName(properties.getProperty("driver"));
            return DriverManager.getConnection(url, username, password);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }


}
