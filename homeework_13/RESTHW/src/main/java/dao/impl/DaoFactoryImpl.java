package dao.impl;


import dao.Dao;
import dao.DaoFactory;
import entity.Damage;

public class DaoFactoryImpl implements DaoFactory<Dao> {
    private static final DaoFactoryImpl daoFactory = new DaoFactoryImpl();

    private final Dao<Damage> damageDao = new DamageDaoImpl();

    public static DaoFactoryImpl getDaoFactoryImpl(){
        return daoFactory;
    }

    @Override
    public DamageDaoImpl getDamageDaoImpl() {
        return (DamageDaoImpl) this.damageDao;
    }

}
