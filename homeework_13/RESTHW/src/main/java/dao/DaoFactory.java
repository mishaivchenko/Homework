package dao;


import dao.impl.DamageDaoImpl;

public interface DaoFactory<Dao> {
        DamageDaoImpl getDamageDaoImpl();
}
