package controller;

import entity.Damage;
import service.impl.ServiceFactoryImpl;
import validator.Validator;
import validator.ValidatorImpl;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/damages")
public class DamageController {
    Validator<Damage> validator = new ValidatorImpl();
    Response response;
    @HEAD
    @Produces(MediaType.TEXT_HTML)
    public Response head(){

        return Response.status(Response.Status.OK).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(){ return Response.ok(ServiceFactoryImpl.getServiceFactory().getDamageService().read()).build();
    }
    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@PathParam("id") int id){
     Damage damage = ServiceFactoryImpl.getServiceFactory().getDamageService().read(id);
        if(damage!=null) return Response.ok(damage).build();
        else return Response.status(Response.Status.NOT_FOUND).build();
    }

    @POST
    @Consumes("application/json")
    @Produces(MediaType.APPLICATION_JSON)
    public Response post(Damage damage){
       if (validator.validate(damage)){
           ServiceFactoryImpl.getServiceFactory().getDamageService().create(damage);
            return Response.status(Response.Status.CREATED).build();
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }

    @PUT
    @Consumes("application/json")
    public Response put(Damage... mas){
        for (Damage damage: mas) {
            if(validator.validate(damage)) {
                ServiceFactoryImpl.getServiceFactory().getDamageService().update(damage);
            }
            return Response.status(Response.Status.ACCEPTED).build();
            }
            return Response.status(Response.Status.BAD_REQUEST).build();
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") int id){
        if(ServiceFactoryImpl.getServiceFactory().getDamageService().read(id)!=null) {
            ServiceFactoryImpl.getServiceFactory().getDamageService().delete(id);
            return Response.status(Response.Status.ACCEPTED).build();
        } else return Response.status(Response.Status.NOT_FOUND).build();
    }

}
