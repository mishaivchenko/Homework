package Validator;

import entity.User;

import javax.servlet.http.HttpServletRequest;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserValidatorImpl implements Validator<HttpServletRequest> {
    private Pattern pattern;
    private Matcher matcher;

    private static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final String PASSWORD_PATTERN="^[_A-Za-z0-9]{5,}$";
    private static final String USERNAME_PATTERN = "^[_A-Za-z0-9]{5,}$";
    @Override
    public boolean validate(HttpServletRequest request) {
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String name = request.getParameter("username");
        return checkEmail(email)&&checkPassword(password)&&checkUserName(name);
    }
    private boolean checkUserName(String username){
        pattern = Pattern.compile(USERNAME_PATTERN);
        matcher = pattern.matcher(username);
        System.out.println(matcher.matches() + " userCheck" );
        return matcher.matches();
    }

    private boolean checkPassword(String password){
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        System.out.println(matcher.matches() + "PasswordCheck" );
        return matcher.matches();
    }
    private boolean checkEmail(String email){
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher  =pattern.matcher(email);
        System.out.println(matcher.matches() + " EmailCheck" );
        return matcher.matches();
    }
}
