package Utill;

import java.sql.*;

public class DbUtillForTest {
    private static final String USERNAME = "root";
    private static final String PASSWORD = "root";
    private static final String URL = "jdbc:mariadb://localhost:3306/pet_project?createDatabaseIfNotExist=true&useUnicode=true&characterEncoding=utf-8";
    private Connection connection;
    private Statement statement;
    PreparedStatement ps;

    public DbUtillForTest() {
        try {
            Class.forName("org.mariadb.jdbc.Driver");
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    public void close() throws SQLException {
        connection.close();
    }
    public void rollback() throws SQLException {
        connection.rollback();
    }

    public PreparedStatement getPs() {
        return ps;
    }
    public void setPs(PreparedStatement ps){
        this.ps = ps;
    }

    public Statement getStatement () {
        return statement;
    }
    public Connection getConnection () {
        return connection;
    }
}
