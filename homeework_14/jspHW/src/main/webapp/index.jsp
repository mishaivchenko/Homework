<html>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<head>
    <meta charset="utf-8">
</head>

<body>

<c:choose>
<c:when test="${username != null}">
<ctg:hello name="${username}"/>
<h1>user added to db</h1>
</c:when>
   <c:otherwise>
       <h2>Hello, you at first page</h2>
   </c:otherwise>
</c:choose>
<ul>
    <li><a href="registration.jsp"><h3>go to registration page</h3></a></li>
</ul>
</body>
</html>
