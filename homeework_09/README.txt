
This program includes 6 implementations of design patterns.

1. Builder

The basis was taken by the Lenovo concern. It is known that not so long ago the company bought out the concern Motorolla. 
The idea is that when assembling devices (in this example, mobile phones) robots manipulators do not know the sequence of assembly and nuances of devices.
For this, there is a class director who gives a clear build algorithm for each Lenovo and Motorolla device.
public class Director {
    public Phone createLonovoPhone(Builder builder){
        return builder
                .buildCaseMaterial(CaseMaterial.METAL)
                .buildCPU("Qualcom")
                .buildGPU("Qualcom")
                .buildCamera("Sony 10 mpx")
                .buildBatteryCapacity(3000)
                .buildTouchId(true)
                .buildScreen("Amoled").buildBrand(Brands.LENOVO).buildModel("nimbus 3000").build();
        }
        public Phone createMotorollaPhone(Builder builder){
            return builder
                    .buildCaseMaterial(CaseMaterial.METAL)
                    .buildCPU("MediaTek")
                    .buildGPU("MediaTek")
                    .buildCamera("Sony 8 mpx")
                    .buildBatteryCapacity(3000)
                    .buildTouchId(true)
                    .buildScreen("IPS").buildBrand(Brands.MOTOROLLA).buildModel("moto").build();
        }
}

2. Command

For a basis any shop in which behind cash department the person works is taken. Since a man is prone to make mistakes, there is the option of canceling the last action.
 So in my implementation there is an opportunity to add goods to the check and apply a discount. 
So if, for some reason, the added product was superfluous, instead of shouting "������, ���������" it is possible to press the cancel button.


3. Decorator 


To implement the decorator pattern, I chose a car sales store. 
To implement the decorator pattern, I chose a car sales store. So when buying a car, you can add additional items to it. to the example of a heated seat or air conditioner.

4. Factory method

To implement the pattern of the factory method, I chose the publishing house of books, which publishes books for different countries. 
The meaning lies in the fact that different countries have their differences that can manifest themselves in the peculiarities of the language, censorship restrictions, etc. 
So with the use of various implementations of the factory, we can receive a book for a specific country.
      Book uaBook = uaBookPublishingHouse.publishABook("³�����: ������� �������","������ �����������");
      Book enBook = englandBookPublishingHouse.publishABook("The Witcher:The Last Wish", "Andrzej Sapkowski" );
      Book usaBook = usaBookPublishingHouse.publishABook("The Witcher:Blood of Elves","Andrzej Sapkowski");


5. Strategy
in this case, as a basis, I took a restaurant for the preparation of such a wonderful dish as a hamburger. 
The bottom line is that for each variety of this dish there is a strategy of preparation. So choosing a specific strategy, we get the result we need.


6. Teplate method 

To implement this pattern, I took an abstract factory for the production of airplanes, boats and cars. So the general parties were singled out the algorithm which is available at all listed means of transportation was made. 
So to build a car you can use a common template in which only a few characteristic details are added.