package strategy;

import strategy.strategies.CheeseburgerCookStrategy;
import strategy.strategies.FishburgerCookStrategy;

public class Main {
    public static void main(String[] args) {
        CheeseburgerCookStrategy cheeseburgerCookStrategy = new CheeseburgerCookStrategy();
        FishburgerCookStrategy fishburgerCookStrategy = new FishburgerCookStrategy();

        Burger burger = fishburgerCookStrategy.cookBurger();
        System.out.println(burger);
    }
}
