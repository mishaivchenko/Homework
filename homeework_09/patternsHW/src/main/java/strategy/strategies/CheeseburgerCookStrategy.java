package strategy.strategies;

import strategy.Burger;
import strategy.healperKitchen.BurgerHellKitchen;
import strategy.ingridients.*;

public class CheeseburgerCookStrategy implements CookStrategy {
    BurgerHellKitchen burgerHellKitchen;
    @Override
    public Burger cookBurger() {
        Burger burger = new Burger();
        burger.setTitle("Cheeseburger");

        burgerHellKitchen = new BurgerHellKitchen(burger);
            burgerHellKitchen.addBreadBasis(new BreadBasis("bun"));
            burgerHellKitchen.addCheese(new Cheese("incredible cheese"));
            burgerHellKitchen.addCutlet(new Cutlet("beef cutlet"));
            burgerHellKitchen.addVegetable(new Vegetable("cucumber"));
            burgerHellKitchen.addPepper(new Pepper("black pepper"));
            burgerHellKitchen.addSauce(new Sauce("ketchup"));
            System.out.println("Your" + burger.getTitle() + "is ready");
            return burger;
    }
}
