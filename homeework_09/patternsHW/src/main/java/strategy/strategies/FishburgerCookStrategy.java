package strategy.strategies;

import strategy.Burger;
import strategy.healperKitchen.BurgerHellKitchen;
import strategy.ingridients.*;

public class FishburgerCookStrategy implements CookStrategy {
    BurgerHellKitchen burgerHellKitchen;
    @Override
    public Burger cookBurger() {
        Burger burger = new Burger();
        burger.setTitle("Fishburger");

        burgerHellKitchen = new BurgerHellKitchen(burger);

        burgerHellKitchen.addBreadBasis(new BreadBasis("bun"));
        burgerHellKitchen.addCutlet(new Cutlet("Fish cutlet"));
        burgerHellKitchen.addVegetable(new Vegetable("tomato"));
        burgerHellKitchen.addSalad(new Salad("salad"));
        burgerHellKitchen.addPepper(new Pepper("black pepper"));
        burgerHellKitchen.addSauce(new Sauce( "mayonnaise"));
        System.out.println("Your" + burger.getTitle() + "is ready");
        return burger;
    }
}
