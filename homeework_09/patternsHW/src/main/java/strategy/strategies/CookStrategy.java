package strategy.strategies;

import strategy.Burger;

public interface CookStrategy {
    Burger cookBurger();
}
