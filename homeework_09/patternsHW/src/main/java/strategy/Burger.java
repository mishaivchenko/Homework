package strategy;

import strategy.ingridients.Ingredient;

import java.util.ArrayList;
import java.util.List;

public class Burger {
    private String title;
    private List<Ingredient> ingredients;
    public Burger(){
        ingredients = new ArrayList<>();
    }

    public void setTitle(String title){
        this.title = title;
    }
    public String getTitle(){
        return title;
    }

    public void addIngridient(Ingredient ingridient){
        System.out.println(ingridient + " ingridient");
        ingredients.add(ingridient);
    }

    @Override
    public String toString() {
        return "Burger{" +
                "title='" + title + '\'' +
                ", ingredients=" + ingredients +
                '}';
    }
}
