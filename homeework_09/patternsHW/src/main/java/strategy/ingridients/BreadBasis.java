package strategy.ingridients;

public class BreadBasis extends Ingredient{

    public BreadBasis(String name) {
        super(name);
    }
    public String getName(){
        return this.name;
    }

    @Override
    public String toString() {
        return name  ;
    }
}
