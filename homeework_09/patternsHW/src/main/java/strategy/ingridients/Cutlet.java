package strategy.ingridients;

public class Cutlet extends Ingredient {
    String meatType;


    public Cutlet(String name) {
        super(name);

    }

  public String getName(){
        return this.name;
  }

    @Override
    public String toString() {
        return name  ;
    }
}
