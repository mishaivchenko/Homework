package strategy.ingridients;

public class Pepper extends Ingredient {

    public Pepper(String name) {
        super(name);
        }
    public String getName(){
        return this.name;
    }
    @Override
    public String toString() {
        return name  ;
    }
}
