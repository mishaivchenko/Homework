package strategy.ingridients;

public class Salad extends Ingredient {

    public Salad(String name) {
        super(name);
    }
    public String getName(){
        return this.name;
    }
    @Override
    public String toString() {
        return name  ;
    }

}
