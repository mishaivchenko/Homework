package strategy.ingridients;

public class Vegetable extends Ingredient {

    public Vegetable(String name) {
        super(name);
    }
    public String getName(){
        return this.name;
    }
    @Override
    public String toString() {
        return name  ;
    }
}
