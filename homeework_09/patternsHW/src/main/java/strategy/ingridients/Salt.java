package strategy.ingridients;

public class Salt extends Ingredient {
    public Salt(String name) {
        super(name);
    }
    public String getName(){
        return this.name;
    }
    @Override
    public String toString() {
        return name  ;
    }
}
