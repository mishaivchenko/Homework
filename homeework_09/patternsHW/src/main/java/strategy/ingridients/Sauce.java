package strategy.ingridients;

public class Sauce extends Ingredient {

    public Sauce(String name) {
        super(name);
    }
    public String getName(){
        return this.name;
    }
    @Override
    public String toString() {
        return name  ;
    }
}
