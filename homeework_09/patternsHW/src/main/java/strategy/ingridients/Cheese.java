package strategy.ingridients;

public class Cheese extends Ingredient {
    public Cheese(String name) {
        super(name);
    }
    public String getName(){
        return this.name;
    }
    @Override
    public String toString() {
        return name  ;
    }
}
