package strategy.healperKitchen;

import strategy.Burger;
import strategy.ingridients.*;

public class BurgerHellKitchen implements HellKitchen {
    Burger burger;

    public BurgerHellKitchen(Burger burger){
        this.burger = burger;
    }

    @Override
    public void addSauce(Sauce sauce) {
        System.out.println("add " + sauce.getName() + " in " + burger.getTitle() + "\n");
        this.burger.addIngridient(sauce);

    }

    @Override
    public void addSalad(Salad salad) {
        System.out.println("add " + salad.getName()+ " in " + burger.getTitle()+ "\n");
        this.burger.addIngridient(salad);
    }

    @Override
    public void addBreadBasis(BreadBasis breadBasis) {

        System.out.println("add " + breadBasis.getName() + " in " + burger.getTitle()+ "\n");
        this.burger.addIngridient(breadBasis);
    }

    @Override
    public void addCutlet(Cutlet cutlet) {
        System.out.println("add " + cutlet.getName() + " in " + burger.getTitle()+ "\n");
        this.burger.addIngridient(cutlet);
    }

    @Override
    public void addVegetable(Vegetable vegetable) {
        System.out.println("add " + vegetable.getName() + " in " + burger.getTitle()+ "\n");
        this.burger.addIngridient(vegetable);
    }

    @Override
    public void addCheese(Cheese cheese) {
        System.out.println("add " + cheese.getName() + " in " + burger.getTitle()+ "\n");
        this.burger.addIngridient(cheese);
    }

    @Override
    public void addSalt(Salt salt) {
        System.out.println("add " + salt.getName() + " in " + burger.getTitle()+ "\n");
        this.burger.addIngridient(salt);
    }

    @Override
    public void addPepper(Pepper pepper) {
        System.out.println("add " + pepper.getName() + " in " + burger.getTitle()+ "\n");
        this.burger.addIngridient(pepper);
    }
}
