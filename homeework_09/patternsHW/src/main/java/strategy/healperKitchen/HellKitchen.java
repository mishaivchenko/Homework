package strategy.healperKitchen;

import strategy.ingridients.*;

public interface HellKitchen {
    void addSauce(Sauce sauce);
    void addSalad(Salad salad);
    void addBreadBasis(BreadBasis breadBasis);
    void addCutlet(Cutlet cutlet);
    void addVegetable(Vegetable vegetable);
    void addCheese(Cheese cheese);
    void addSalt(Salt salt);
    void addPepper(Pepper pepper);
}
