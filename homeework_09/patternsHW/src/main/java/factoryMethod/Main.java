package factoryMethod;

import factoryMethod.book.Book;
import factoryMethod.factory.BookPublishingHouse;
import factoryMethod.factory.EnglandbookPublishingHouse;
import factoryMethod.factory.UAbookPublishingHouse;
import factoryMethod.factory.USAbookPublishingHouse;

public class Main {
    public static void main(String[] args) {
        BookPublishingHouse uaBookPublishingHouse = new UAbookPublishingHouse();
        BookPublishingHouse usaBookPublishingHouse = new USAbookPublishingHouse();
        BookPublishingHouse englandBookPublishingHouse = new EnglandbookPublishingHouse();

        Book uaBook = uaBookPublishingHouse.publishABook("Відьмак: Останнє бажання","Анджей Сапковський");
        System.out.println();
        Book enBook = englandBookPublishingHouse.publishABook("The Witcher:The Last Wish", "Andrzej Sapkowski" );
        System.out.println();
        Book usaBook = usaBookPublishingHouse.publishABook("The Witcher:Blood of Elves","Andrzej Sapkowski");
    }
}
