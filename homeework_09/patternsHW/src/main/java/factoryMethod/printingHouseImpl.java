package factoryMethod;

import factoryMethod.book.Book;

public class printingHouseImpl implements printingHouse {
    public void printBook(Book book) {

        System.out.println(book.title + " prepare to print");
        System.out.println(".......  \n" + ".......");
        System.out.println(book + " successfully printed");
    }
}
