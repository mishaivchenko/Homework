package factoryMethod.factory;

import factoryMethod.book.Book;
import factoryMethod.book.UAbook;

public class UAbookPublishingHouse extends BookPublishingHouse {


    public Book createABook() {
        return new UAbook();
    }
}
