package factoryMethod.factory;

import factoryMethod.book.Book;
import factoryMethod.book.EnglandBook;

public class EnglandbookPublishingHouse extends BookPublishingHouse {
    protected Book createABook() {
        return new EnglandBook();
    }
}
