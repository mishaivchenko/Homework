package factoryMethod.factory;

import factoryMethod.book.Book;
import factoryMethod.book.USAbook;

public class USAbookPublishingHouse extends BookPublishingHouse {

    protected Book createABook() {
        return new USAbook();
    }
}
