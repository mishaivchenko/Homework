package factoryMethod.factory;

import factoryMethod.book.Book;
import factoryMethod.printingHouse;
import factoryMethod.printingHouseImpl;

public abstract class BookPublishingHouse {
    Book book ;
    factoryMethod.printingHouse printingHouse = new printingHouseImpl();
    public Book publishABook(String title, String authorName){
        book = createABook();
        book.setAuthorName(authorName);
        book.setTitle(title);
        printingHouse.printBook(book);
        return book;
    }

    protected abstract Book createABook();
}
