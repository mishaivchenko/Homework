package factoryMethod;

import factoryMethod.book.Book;

public interface printingHouse {
    void printBook(Book book);

}
