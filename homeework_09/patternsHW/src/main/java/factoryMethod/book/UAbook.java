package factoryMethod.book;

public class UAbook extends Book {

    public UAbook(String title, String authorName){
        this.title = title;
        this.authorName = authorName;
        this.censureCategory = 7;
        this.language = "Ukrainian";
        this.country = "Ukaine";
    }

  public  UAbook(){
        this.censureCategory = 7;
        this.language = "Ukrainian";
        this.country = "Ukaine";
    }

}
