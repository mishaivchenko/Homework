package factoryMethod.book;

public class EnglandBook extends Book {
    public EnglandBook(String title, String authorName){
        this.title = title;
        this.authorName = authorName;
        this.censureCategory = 5;
        this.language = "England_English";
        this.country = "England";
    }

    public  EnglandBook(){
        this.censureCategory = 5;
        this.language = "England_English";
        this.country = "England";
    }

}
