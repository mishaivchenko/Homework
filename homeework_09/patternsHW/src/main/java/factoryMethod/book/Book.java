package factoryMethod.book;

public abstract class Book {
    public String title;
    public String authorName;
    public String country;
    public String language;
    public int censureCategory;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                ", authorName='" + authorName + '\'' +
                ", country='" + country + '\'' +
                ", language='" + language + '\'' +
                ", censureCategory=" + censureCategory +
                '}';
    }
}
