package factoryMethod.book;


public class USAbook extends Book {
    public USAbook(String title, String authorName){
        this.title = title;
        this.authorName = authorName;
        this.censureCategory = 4;
        this.language = "USA_English";
        this.country = "USA";
    }

    public USAbook(){
        this.censureCategory = 4;
        this.language = "USA_English";
        this.country = "USA";
    }
}
