package command.commands;


import command.bill.Bill;
import command.pojo.Product;

public class AddProductCommand implements Command {
    private Bill bill;
    private Product product;
    private  Product saveProduct;

    public AddProductCommand(Bill bill, Product product){
        this.bill = bill;
        this.product = product;
        this.saveProduct = product;
    }

    @Override
    public void execute() {
        bill.addProduct(product);
    }

    @Override
    public void unDo() {
    bill.removeProduct(saveProduct);

    }
}
