package command.commands;

import command.bill.Bill;

public class GetDiscountCommand implements Command {

    private double discount;
    private Bill bill;
    private double sum;

    public GetDiscountCommand(Bill bill, double discount){
        this.bill = bill;
        this.discount = discount;
        sum = this.bill.getSum();
    }
    @Override
    public void execute() {
        this.bill.applyDiscount(discount);
    }

    @Override
    public void unDo() {
        this.bill.cancelDiscount(sum);
    }
}
