package command;

import command.bill.Bill;
import command.bill.BillImpl;
import command.cashRegister.CashRegister;
import command.cashRegister.CashRegisterImpl;
import command.commands.AddProductCommand;
import command.commands.Command;
import command.commands.GetDiscountCommand;
import command.pojo.Product;

public class Main {
    public static void main(String[] args) {
        Bill bill = new BillImpl();
        CashRegister cashRegister = new CashRegisterImpl();
        AddProductCommand addProductCommand = new AddProductCommand(bill,new Product("bread",10.0));

        cashRegister.setCommand(addProductCommand);
        cashRegister.buttonWasPressed();
        Command getDiscountCommand = new GetDiscountCommand(bill,20);
        cashRegister.setCommand(getDiscountCommand);
        cashRegister.buttonWasPressed();
        System.out.println(bill.getSum());
        addProductCommand = new AddProductCommand(bill,new Product("sob",42.0));
        cashRegister.setCommand(addProductCommand);
        cashRegister.buttonWasPressed();
        System.out.println(bill.getSum());
        cashRegister.undoButtonWasPressed();
        System.out.println(bill.getSum());
        System.out.println(bill.getSum());
        getDiscountCommand = new GetDiscountCommand(bill,10);
        cashRegister.setCommand(getDiscountCommand);
        cashRegister.buttonWasPressed();
        System.out.println(bill.getSum());
        cashRegister.undoButtonWasPressed();
        System.out.println(bill.getSum());
    }
}
