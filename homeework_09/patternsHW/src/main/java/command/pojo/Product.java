package command.pojo;

public class Product {
   private String title;
    private double cost;

    public Product(String title, double cost) {
        this.title = title;
        this.cost = cost;
    }

    public double getCost() {
        return cost;
    }
}
