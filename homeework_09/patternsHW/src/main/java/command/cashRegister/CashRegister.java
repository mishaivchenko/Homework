package command.cashRegister;

import command.commands.Command;

public interface CashRegister {
    void setCommand(Command command);
    void buttonWasPressed();
    void undoButtonWasPressed();
}
