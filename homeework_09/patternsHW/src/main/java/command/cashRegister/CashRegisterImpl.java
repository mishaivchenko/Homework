package command.cashRegister;

import command.commands.Command;

public class CashRegisterImpl implements CashRegister{
private Command command;

    public CashRegisterImpl() {
    }

    @Override
    public void setCommand(Command command) {
    this.command = command;
    }

    @Override
    public void buttonWasPressed() {
        this.command.execute();
    }

    @Override
    public void undoButtonWasPressed() {
        this.command.unDo();
    }
}
