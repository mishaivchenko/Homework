package command.bill;

import command.pojo.Product;

public interface Bill {
   void addProduct(Product product);
   double getSum();
    void applyDiscount(double discount);
    void removeProduct(Product product);
    void cancelDiscount(double sum);
}
