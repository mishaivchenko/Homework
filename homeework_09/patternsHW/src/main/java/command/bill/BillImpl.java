package command.bill;

import command.pojo.Product;

import java.util.ArrayList;
import java.util.List;

public class BillImpl implements Bill {
    private double sum;
    private List<Product> productsList;

    public BillImpl(){
        productsList = new ArrayList<>();
    }
    @Override
    public void addProduct(Product product) {
    productsList.add(product);
    sum+=product.getCost();
    }

    @Override
    public double getSum() {
        return sum;
    }

    @Override
    public void applyDiscount(double discount) {
        sum = sum - sum*(discount/100);
    }

    @Override
    public void removeProduct(Product product) {
        if (product!=null||productsList.contains(product)) {
            productsList.remove(product);
            double s =  sum-  product.getCost();
            if (s>0) sum = s; else sum = 0;
        }
    }

    public void cancelDiscount(double sum){
        this.sum = sum;
    }
}
