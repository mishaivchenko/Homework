package decorator;

import decorator.Car.Car;
import decorator.Car.SkodaCar;
import decorator.additions.HeatedSeats;
import decorator.additions.Parktronic;

public class main {
    public static void main(String[] args) {
        Car car = new SkodaCar();
        System.out.println(car.name() + " " + car.cost());

        car = new HeatedSeats(car);
        car = new Parktronic(car);
       // car = new ChromeWheels(car);
        System.out.println(car.name() + " " +  car.cost());
    }


}
