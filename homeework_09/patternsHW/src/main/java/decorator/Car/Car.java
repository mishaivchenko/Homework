package decorator.Car;

public interface Car {
    double cost();
    String name();
}
