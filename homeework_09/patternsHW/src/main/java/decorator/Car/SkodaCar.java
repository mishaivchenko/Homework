package decorator.Car;

public class SkodaCar implements Car {
   private String name = "Skoda";
    private double cost = 20000;


    public double cost() {
        return this.cost;
    }

    public String name() {
        return this.name;
    }

}
