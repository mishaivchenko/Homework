package decorator;

import decorator.Car.Car;

public interface AdditionsDecorator extends Car {
    public String name();

}
