package decorator.additions;

import decorator.AdditionsDecorator;
import decorator.Car.Car;

public class ChromeWheels implements AdditionsDecorator {

    private Car car;
    private String addName = "Chrome wheels";
    private double cost = 250;

    public ChromeWheels(Car car){
        this.car = car;
    }

    @Override
    public double cost() {
        return car.cost() + this.cost;
    }

    @Override
    public String name() {
        return car.name() +", " + this.addName;
    }
}
