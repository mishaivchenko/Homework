package decorator.additions;

import decorator.AdditionsDecorator;
import decorator.Car.Car;

public class HeatedSeats implements AdditionsDecorator {

    private Car car;
    private String addName = "HeatedSeats";
    private double cost = 600;

    public HeatedSeats(Car car){
        this.car = car;
    }

    @Override
    public double cost() {
        return car.cost() + this.cost;
    }

    @Override
    public String name() {
        return car.name() +", " + this.addName;
    }
}
