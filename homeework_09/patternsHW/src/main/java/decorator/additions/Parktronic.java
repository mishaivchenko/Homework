package decorator.additions;

import decorator.AdditionsDecorator;
import decorator.Car.Car;

public class Parktronic implements AdditionsDecorator {
    private Car car;
    private String addName = "parktronic";
    private double cost = 1000;



    public Parktronic(Car car){
        this.car = car;
    }

    @Override
    public double cost() {
        return car.cost() + this.cost;
    }

    @Override
    public String name() {
        return car.name() +", " + this.addName;
    }
}
