package decorator.additions;

import decorator.AdditionsDecorator;
import decorator.Car.Car;

public class AirConditioning implements AdditionsDecorator {
    private Car car;
    private String addName = "air conditioning";
    private double cost = 770;

    public AirConditioning(Car car){
    this.car = car;
    }

    @Override
    public double cost() {
        return car.cost() + this.cost;
    }

    @Override
    public String name() {
        return car.name() +", " + this.addName;
    }
}
