package builder;

import builder.enums.Brands;
import builder.enums.CaseMaterial;

public class PhoneBuilder extends Builder {
    private CaseMaterial caseMaterial = CaseMaterial.PLASTIC;
    private Brands brands = Brands.NONE;
    private  String model = "unknown";
    private String CPU = "unknown";
    private String screen = "unknown";
    private int batteryCapacity = 2500;
    private String camera = "unknown";
    private  boolean touchID = false;
    private String GPU = "unknown";



    @Override
    PhoneBuilder buildCPU(String CPU) {
      this.CPU = CPU;
      return this;
    }

    @Override
    PhoneBuilder buildScreen(String Screen) {
        this.screen = Screen;
        return this;
    }

    @Override
    PhoneBuilder buildBatteryCapacity(int capacity) {
        this.batteryCapacity = capacity;
        return this;
    }

    @Override
    PhoneBuilder buildCamera(String camera) {
        this.camera = camera;
        return this;
    }

    @Override
    PhoneBuilder buildTouchId(boolean touchId) {
        this.touchID = touchId;
        return this;
    }

    @Override
    PhoneBuilder buildGPU(String GPU) {
        this.GPU = GPU;
        return this;
    }

    @Override
    PhoneBuilder buildBrand(Brands brands) {
        this.brands = brands;
        return this;
    }

    @Override
    PhoneBuilder buildCaseMaterial(CaseMaterial caseMaterial) {
        this.caseMaterial = caseMaterial;
        return this;
    }

    @Override
    Builder buildModel(String model) {
        this.model =model;
        return this;
    }

    @Override
    Phone build() {
        return new Phone(caseMaterial,brands,model,CPU,screen,batteryCapacity,camera,touchID,GPU);
    }
}
