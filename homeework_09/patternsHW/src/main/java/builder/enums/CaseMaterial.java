package builder.enums;

public enum CaseMaterial {

    PLASTIC, METAL, GLASS
}
