package builder;

import builder.enums.Brands;
import builder.enums.CaseMaterial;

abstract class Builder {
        Phone phone;
       //abstract Builder createPhone();


        abstract Builder buildCPU(String CPU);
        abstract Builder buildScreen(String Screen);
        abstract Builder buildBatteryCapacity(int capacity);
        abstract Builder buildCamera(String camera);
        abstract Builder buildTouchId(boolean touchId);
        abstract Builder buildGPU(String GPU);
        abstract Builder buildBrand(Brands brands);
        abstract Builder buildCaseMaterial(CaseMaterial caseMaterial);
        abstract Builder buildModel(String model);

        abstract Phone build();


    }

