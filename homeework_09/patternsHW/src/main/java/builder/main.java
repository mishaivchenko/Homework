package builder;

public class main {
    public static void main(String[] args) {
    Director director = new Director();
    Builder builder = new PhoneBuilder();
    director.createLonovoPhone(builder);
    Phone lenovoPhone = builder.build();
        System.out.println(lenovoPhone);

        Phone motorolla =  director.createMotorollaPhone(builder);
        System.out.println(motorolla);

    }

}
