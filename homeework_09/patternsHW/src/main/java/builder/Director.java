package builder;


import builder.enums.Brands;
import builder.enums.CaseMaterial;

public class Director {

    public Phone createLonovoPhone(Builder builder){
        return builder
                .buildCaseMaterial(CaseMaterial.METAL)
                .buildCPU("Qualcom")
                .buildGPU("Qualcom")
                .buildCamera("Sony 10 mpx")
                .buildBatteryCapacity(3000)
                .buildTouchId(true)
                .buildScreen("Amoled").buildBrand(Brands.LENOVO).buildModel("nimbus 3000").build();
        }
        public Phone createMotorollaPhone(Builder builder){
            return builder
                    .buildCaseMaterial(CaseMaterial.METAL)
                    .buildCPU("MediaTek")
                    .buildGPU("MediaTek")
                    .buildCamera("Sony 8 mpx")
                    .buildBatteryCapacity(3000)
                    .buildTouchId(true)
                    .buildScreen("IPS").buildBrand(Brands.MOTOROLLA).buildModel("moto").build();
        }
}
