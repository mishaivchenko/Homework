package builder;

import builder.enums.Brands;
import builder.enums.CaseMaterial;

public class Phone {


    private CaseMaterial caseMaterial;
    private Brands brands ;
    private  String model;
    private String CPU;
    private String screen;
    private int batteryCapacity = 0;
    private String camera;
    private  boolean touchID;
    private String GPU;


    public Phone(CaseMaterial caseMaterial, Brands brands, String model, String CPU, String screen, int batteryCapacity, String camera, boolean touchID, String GPU) {
        this.caseMaterial = caseMaterial;
        this.brands = brands;
        this.model = model;
        this.CPU = CPU;
        this.screen = screen;
        this.batteryCapacity = batteryCapacity;
        this.camera = camera;
        this.touchID = touchID;
        this.GPU = GPU;
    }


    public CaseMaterial getCaseMaterial() {
        return caseMaterial;
    }

    public void setCaseMaterial(CaseMaterial caseMaterial) {
        this.caseMaterial = caseMaterial;
    }

    public Brands getBrands() {
        return brands;
    }

    public void setBrands(Brands brands) {
        this.brands = brands;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getCPU() {
        return CPU;
    }

    public void setCPU(String CPU) {
        this.CPU = CPU;
    }

    public String getScreen() {
        return screen;
    }

    public void setScreen(String screen) {
        this.screen = screen;
    }

    public int getBatteryCapacity() {
        return batteryCapacity;
    }

    public void setBatteryCapacity(int batteryCapacity) {
        this.batteryCapacity = batteryCapacity;
    }

    public String getCamera() {
        return camera;
    }

    public void setCamera(String camera) {
        this.camera = camera;
    }

    public boolean isTouchID() {
        return touchID;
    }

    public void setTouchID(boolean touchID) {
        this.touchID = touchID;
    }

    public String getGPU() {
        return GPU;
    }

    public void setGPU(String GPU) {
        this.GPU = GPU;
    }

    @Override
    public String toString() {
        return "Phone{" +
                "caseMaterial=" + caseMaterial +
                ", brands=" + brands +
                ", model='" + model + '\'' +
                ", CPU='" + CPU + '\'' +
                ", screen='" + screen + '\'' +
                ", batteryCapacity=" + batteryCapacity +
                ", camera='" + camera + '\'' +
                ", touchID=" + touchID +
                ", GPU='" + GPU + '\'' +
                '}';
    }
}
