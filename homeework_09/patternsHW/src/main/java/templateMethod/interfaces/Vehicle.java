package templateMethod.interfaces;

public interface Vehicle {
    void buildBody();
    void engineMounting();
    void powerSupplySystemMounting();
    void mechanicalPartsMounting();
    void electronicPartsMounting();
    void interiorDesign();
    void deploy();
}
