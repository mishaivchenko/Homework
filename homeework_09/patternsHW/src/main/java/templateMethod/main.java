package templateMethod;

import templateMethod.Impl.CarVehicleImpl;
import templateMethod.interfaces.Vehicle;

public class main {
    public static void main(String[] args) {
        Vehicle car = new CarVehicleImpl();
        car.deploy();
    }


}
