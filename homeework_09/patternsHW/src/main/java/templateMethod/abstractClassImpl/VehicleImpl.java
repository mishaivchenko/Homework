package templateMethod.abstractClassImpl;

import templateMethod.interfaces.Vehicle;

public abstract class VehicleImpl implements Vehicle {


    @Override
    public abstract void buildBody();

    @Override
    public  abstract void engineMounting();

    @Override
    public abstract void powerSupplySystemMounting();

    @Override
    public abstract void mechanicalPartsMounting();

    @Override
    public void electronicPartsMounting() {
        System.out.println("electronics installation");
    }

    @Override
    public void interiorDesign() {
        System.out.println("installation of seats, floor and ceiling lining");
    }


    @Override
    public void deploy() {
    buildBody();
    engineMounting();
    powerSupplySystemMounting();
    mechanicalPartsMounting();
    electronicPartsMounting();
    interiorDesign();
    }
}
