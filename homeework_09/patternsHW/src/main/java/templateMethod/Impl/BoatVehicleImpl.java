package templateMethod.Impl;

import templateMethod.abstractClassImpl.VehicleImpl;

public class BoatVehicleImpl extends VehicleImpl {


    @Override
    public void buildBody() {
        System.out.println("creation boat body");
    }

    @Override
    public void engineMounting() {
        System.out.println("combustion engine installation in the boat");
    }

    @Override
    public void powerSupplySystemMounting() {
        System.out.println("mounting of boat power supply system");
    }

    @Override
    public void mechanicalPartsMounting() {
        hydrofoilInstallation();
        pumpInstallation();
    }
    public void hydrofoilInstallation(){
        System.out.println("mounting hydrofoil in the boat");
    }
    public void pumpInstallation(){
        System.out.println("mounting pump system in the boat");
    }
}
