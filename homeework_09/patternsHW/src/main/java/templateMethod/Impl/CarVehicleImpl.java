package templateMethod.Impl;

import templateMethod.abstractClassImpl.VehicleImpl;

public class CarVehicleImpl extends VehicleImpl {


    @Override
    public void buildBody() {
        System.out.println("creation car body");
    }

    @Override
    public void engineMounting() {
        System.out.println("combustion engine installation");
    }

    @Override
    public void powerSupplySystemMounting() {
        System.out.println("installation of car power supply system");
    }

    @Override
    public void mechanicalPartsMounting() {
        mountingCarrier();
        mountingTransmission();
    }


    public void mountingTransmission(){
        System.out.println("mounting transmission in the car");
    }
    public  void mountingCarrier(){
        System.out.println("mounting carrier in the car");
    }
}
