package templateMethod.Impl;

import templateMethod.abstractClassImpl.VehicleImpl;

public class AircraftVehicleImpl extends VehicleImpl {



    @Override
    public void buildBody() {
        System.out.println("assembly of the hull of the aircraft, installation of wings and other parts of the hull");
    }

    @Override
    public void engineMounting() {
        System.out.println("engine and turbine installation");
    }

    @Override
    public void powerSupplySystemMounting() {
        System.out.println("installation of aircraft power supply system");
    }

    @Override
    public void mechanicalPartsMounting() {
        chassisInstallation();
        flapInstallation();
        interceptorInstallation();
    }

    public void chassisInstallation(){
        System.out.println("chassis system installation");
    }
    public void flapInstallation(){
        System.out.println("mounting flap in the aircraft");
    }

    public void interceptorInstallation(){
        System.out.println("mounting interceptor in the aircraft");
    }



}
