import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
@WebServlet(name = "echo", urlPatterns = "/echo")
public class EchoServer extends HttpServlet {

    private String message;
    private int contentLength;


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
      doEcho(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    doEcho(req,resp);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doEcho(req,resp);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doEcho(req,resp);
    }

    @Override
    public void init() throws ServletException {
        //message = "Hello world";
    }

    public void doEcho(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        message = req.getParameter("message");
        contentLength = req.getContentLength();
        StringBuffer URL = req.getRequestURL();
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println("<h1>" +  req.getMethod() + "</h1>");
        out.println("<h1>" + message + "</h1>");
        out.println("<h1>" + "Content length " + req.getContentLengthLong()+"</h1>");
        out.println("<h1>" + "URL " + URL +"</h1>");
        Enumeration headerNames = req.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            Object headerName = headerNames.nextElement();
            String headerValue = req.getHeader((String) headerName);
            out.println(String.format("<h1>%s:%s</h1>", headerName, headerValue));
        }
    }
}
