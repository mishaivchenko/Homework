package org.myframework.exceptions;

public class DesiredDependencyIsMissingException extends RuntimeException {

    public DesiredDependencyIsMissingException(String msg){
        super(msg);
    }
}
