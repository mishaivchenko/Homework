package org.myframework.exceptions;

public class CyclicalDependencyException extends RuntimeException {
    public CyclicalDependencyException(String msg){
        super(msg);
    }
}
