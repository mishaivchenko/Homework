package org.myframework.loaders;

import org.ini4j.Profile;
import org.ini4j.Wini;
import org.myframework.graph.node.Node;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public class IniLoader {
    private Wini wini;

    public IniLoader(InputStream inputStream) {
        try {
            wini = new Wini(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Node> getObjects() {
        Node node;
        List<Node> nodeList = new ArrayList<>();
        if (wini != null) {
            Set<Map.Entry<String, Profile.Section>> winiSet = wini.entrySet();
            for (Map.Entry<String, Profile.Section> m : winiSet) {
                node = new Node();
                node.setId(m.getKey());
                HashMap<String, String> hashMap = new HashMap<>();
                for (Map.Entry<String, String> entry : m.getValue().entrySet()) {
                    hashMap.put(entry.getKey(), entry.getValue());
                    node.setParams(hashMap);
                }
                nodeList.add(node);
            }
            return nodeList;
        } else throw new IllegalArgumentException("Something wrong with file.ini");
    }

}