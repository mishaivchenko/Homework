package org.myframework.beans.factory;

import org.myframework.exceptions.DesiredDependencyIsMissingException;
import org.myframework.graph.creator.GraphCreator;
import org.myframework.graph.node.Node;
import org.myframework.loaders.IniLoader;

import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BeanFactory {
    private IniLoader iniLoader;
    private Map<String, Object> singletons = new HashMap<>();

    public BeanFactory(String path) {
        InputStream inputStream = ClassLoader.getSystemResourceAsStream(path);
        iniLoader = new IniLoader(inputStream);
    }

    public Object getBean(String beanName) {
        return singletons.get(beanName);
    }


    public void instantiate() throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        List<Node> nodes = iniLoader.getObjects();
        GraphCreator graphCreator = new GraphCreator(nodes);
        nodes = graphCreator.createGraph();

        for (Node node : nodes) {
            Class newClass = Class.forName(node.getId().substring(0, node.getId().lastIndexOf(":")));
            List<Object> params = new ArrayList();
            for (Constructor constructor : newClass.getConstructors()) {
                for (Parameter parameter : constructor.getParameters()) {
                    String name = node.getParams().get(parameter.getName());
                    if (name == null) {
                        throw new DesiredDependencyIsMissingException("dependency " + parameter.getName() + " is missing");
                    }
                    String value = name.substring(name.lastIndexOf(":") + 1);
                    String paramType = name.substring(0, name.lastIndexOf(":"));
                    if (paramType.compareTo("ref") == 0) {
                        if (singletons.get(value) != null) {
                            params.add(singletons.get(value));
                        }

                    } else if (paramType.compareTo("val") == 0) {
                        parseParam(params, parameter.getType().getName(), value);
                    }
                }
                try {
                    String beanName = node.getId().substring(node.getId().lastIndexOf(":") + 1);
                    Object object = constructor.newInstance(params.toArray());
                    singletons.put(beanName, object);
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void parseParam(List<Object> params, String paramType, String value) {
        switch (paramType) {
            case "int":
                params.add(Integer.parseInt(value));
                break;
            case "java.lang.String":
                params.add(value);
                break;
            case "float":
                params.add(Float.parseFloat(value));
                break;
            case "long":
                params.add(Long.parseLong(value));
                break;
            case "double":
                params.add(Double.parseDouble(value));
                break;
            case "short":
                params.add(Short.parseShort(value));
                break;
            default:
                throw new IllegalArgumentException();
        }
    }
}

