package org.myframework.graph.creator;

import org.myframework.exceptions.CyclicalDependencyException;
import org.myframework.graph.node.Node;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Stack;

public class GraphCreator {
    private Stack<String> stack = new Stack<>();
    private List<Node> nodesDeps = new ArrayList<>();
    private List<Node> nodes;

    public GraphCreator(List<Node> nodes) {
        this.nodes = nodes;
    }


    public List<Node> createGraph() {
        for (Node node : nodes) {
            dependencyAnalyzer(node, new ArrayList<>());
        }
        return nodesDeps;
    }

    private void dependencyAnalyzer(Node nodeToAnalyze, List<String> dependencies) {
        String paramType;
        String refName;
        String beanName;
        if (!withoutRef(nodeToAnalyze.getParams())) {
            for (String s : nodeToAnalyze.getParams().values()) {
                paramType = s.substring(0, s.lastIndexOf(":"));
                if (paramType.compareTo("ref") == 0) {
                    refName = s.substring(s.lastIndexOf(":") + 1);
                    if (!stack.contains(refName)) {
                        for (Node node : nodes) {
                            beanName = node.getId().substring(node.getId().lastIndexOf(":") + 1);
                            if (beanName.compareTo(refName) == 0) {
                               chooseAction(nodeToAnalyze,node, dependencies);
                        }
                        }
                    }
                }
            }
            updateStack(nodeToAnalyze);
        } else {
            updateStack(nodeToAnalyze);
        }
    }



    private boolean withoutRef(Map<String, String> map) {
        for (String s : map.values()) {
            if (s.substring(0, s.lastIndexOf(":")).compareTo("ref") == 0) {
                return false;
            }
        }
        return true;
    }

    private void chooseAction(Node nodeToAnalyze,Node node, List<String> dependencies){
        if (dependencies.size()==0||!dependencies.contains(node.getId())) {
            dependencies.add(node.getId());
            dependencyAnalyzer(node,dependencies);
        } else if (dependencies.contains(node.getId())) {
            throw new CyclicalDependencyException("Cyclical dependency: " + nodeToAnalyze.getId()+ " has dependency ");
        }

    }

    private void updateStack(Node nodeToAnalyze) {
        String name = nodeToAnalyze.getId().substring(nodeToAnalyze.getId().lastIndexOf(":") + 1);
        if (!stack.contains(name)) {
            stack.push(name);
            nodesDeps.add(nodeToAnalyze);
        }
    }


}
