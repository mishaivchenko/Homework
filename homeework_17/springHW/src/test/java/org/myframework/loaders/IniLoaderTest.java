package org.myframework.loaders;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.myframework.graph.node.Node;


import java.io.InputStream;

import java.util.HashMap;
import java.util.List;

public class IniLoaderTest {
   private IniLoader iniLoader;

   @Before
    public void initialize(){
       InputStream inputStream = ClassLoader.getSystemResourceAsStream("settings.ini");
       iniLoader = new IniLoader(inputStream);
   }

   @Test
    public void afterReadIniFileListSizeMustBeSeven(){
       //Given
       int expectedSize = 7;
       List<Node> nodes;
       //When
       nodes = iniLoader.getObjects();
       //Then
       Assert.assertEquals(expectedSize,nodes.size());
   }

   @Test
public void listMustContainsEverySettingFromIniFile(){
       //Given

       Node f1Node = new Node();
       HashMap<String, String> hashMapF1 = new HashMap<>();
       hashMapF1.put("x","val:23");
       hashMapF1.put("str","val:some String");
       f1Node.setParams(hashMapF1);
       f1Node.setId("pojo.F1:f1");

       Node g1Node = new Node();
       HashMap<String, String> hashMapG1 = new HashMap<>();
       hashMapG1.put("f1","ref:f1");
       g1Node.setParams(hashMapG1);
       g1Node.setId("pojo.G1:g1");

       Node g2Node = new Node();
       HashMap<String, String> hashMapG2 = new HashMap<>();
       hashMapG2.put("f1","ref:f1");
       g2Node.setParams(hashMapG2);
       g2Node.setId("pojo.G2:g2");

       Node xNode = new Node();
       HashMap<String, String> hashMapX = new HashMap<>();
       hashMapX.put("h1","ref:h1");
       hashMapX.put("h2","ref:h2");
       hashMapX.put("h3","ref:h3");
       xNode.setParams(hashMapX);
       xNode.setId("pojo.X:x");

       //When
       List<Node> nodes = iniLoader.getObjects();
       //Then
       Assert.assertTrue(nodes.contains(f1Node));
       Assert.assertTrue(nodes.contains(g1Node));
       Assert.assertTrue(nodes.contains(g2Node));
       Assert.assertTrue(nodes.contains(xNode));
   }
}
