package org.myframework.beans.factory;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.myframework.exceptions.CyclicalDependencyException;
import org.myframework.exceptions.DesiredDependencyIsMissingException;
import pojo.F1;
import pojo.G1;

public class BeanFactoryTest {
    private static BeanFactory beanFactory;

    @BeforeClass
    public static void prepareBeanFactory() throws IllegalAccessException, InstantiationException, ClassNotFoundException {
        beanFactory = new BeanFactory("settings.ini");
        beanFactory.instantiate();
    }

    @Test
    public void instanceOfTest() {
        //When
        F1 f1FromContainer = (F1) beanFactory.getBean("f1");
        boolean result = f1FromContainer.getClass() == F1.class;
        //Then
        Assert.assertTrue(result);
    }

    @Test
    public void primitiveAndStringTest() {
        //Given
        int x = 23;
        String str = "some String";
        //When
        F1 f1FromContainer = (F1) beanFactory.getBean("f1");
        //Then
        Assert.assertEquals(f1FromContainer.getX(), x);
        Assert.assertEquals(f1FromContainer.getStr(), str);
    }

    @Test
    public void referenceTest() {
        //Given
        G1 g1 = (G1) beanFactory.getBean("g1");
        //When
        boolean result = g1.getF1().getClass() == F1.class;
        //Then
        Assert.assertTrue(result);
    }

    @Test
    public void singletonTest() {
        //Given
        F1 f1FromContainer = (F1) beanFactory.getBean("f1");
        //When
        F1 anotherF1FromContainer = (F1) beanFactory.getBean("f1");
        //Then
        Assert.assertEquals(f1FromContainer, anotherF1FromContainer);
    }

    @Test(expected = CyclicalDependencyException.class)
    public void cyclicalDependencyTest() throws IllegalAccessException, InstantiationException, ClassNotFoundException {
        BeanFactory beanFactory2 = new BeanFactory("settingsWithCyclicDependency.ini");
        beanFactory2.instantiate();
    }

    @Test(expected = DesiredDependencyIsMissingException.class)
    public void missingDependencyTest() throws IllegalAccessException, InstantiationException, ClassNotFoundException {
        BeanFactory beanFactory3 = new BeanFactory("settingsWithMissingDependency.ini");
        beanFactory3.instantiate();
    }


}
