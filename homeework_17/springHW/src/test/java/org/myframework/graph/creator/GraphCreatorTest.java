package org.myframework.graph.creator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.myframework.exceptions.CyclicalDependencyException;
import org.myframework.graph.node.Node;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GraphCreatorTest {
    private List<Node> nodes;
    private GraphCreator graphCreator;
    @Before
    public void initializeList() {
        Node f1Node = new Node();
        HashMap<String, String> hashMapF1 = new HashMap<>();
        hashMapF1.put("x", "val:23");
        hashMapF1.put("str", "val:some String");
        f1Node.setParams(hashMapF1);
        f1Node.setId("pojo.F1:f1");

        Node g1Node = new Node();
        HashMap<String, String> hashMapG1 = new HashMap<>();
        hashMapG1.put("f1", "ref:f1");
        g1Node.setParams(hashMapG1);
        g1Node.setId("pojo.G1:g1");

        Node g2Node = new Node();
        HashMap<String, String> hashMapG2 = new HashMap<>();
        hashMapG2.put("f1", "ref:f1");
        g2Node.setParams(hashMapG2);
        g2Node.setId("pojo.G2:g2");

        Node h1Node = new Node();
        HashMap<String, String> hashMapH1 = new HashMap<>();
        hashMapH1.put("g1", "ref:g1");
        h1Node.setParams(hashMapH1);
        h1Node.setId("pojo.H1:h1");

        Node h2Node = new Node();
        HashMap<String, String> hashMapH2 = new HashMap<>();
        hashMapH2.put("g1", "ref:g1");
        hashMapH2.put("g2", "ref:g2");
        h2Node.setParams(hashMapH1);
        h2Node.setId("pojo.H2:h2");

        Node h3Node = new Node();
        HashMap<String, String> hashMapH3 = new HashMap<>();
        hashMapH3.put("g2", "ref:g2");
        h3Node.setParams(hashMapH3);
        h3Node.setId("pojo.H1:h3");


        Node xNode = new Node();
        HashMap<String, String> hashMapX = new HashMap<>();
        hashMapX.put("h1", "ref:h1");
        hashMapX.put("h2", "ref:h2");
        hashMapX.put("h3", "ref:h3");
        xNode.setParams(hashMapX);
        xNode.setId("pojo.X:x");

        nodes = new ArrayList<>();
        nodes.add(xNode);
        nodes.add(h2Node);
        nodes.add(f1Node);
        nodes.add(h3Node);
        nodes.add(h1Node);
        nodes.add(g2Node);
        nodes.add(g1Node);

    }

    @Test
    public void sortingTestAfterSortFirstElementMustBeF1() {
        //Given
        graphCreator = new GraphCreator(nodes);
        Node firstElement = new Node();
        HashMap<String, String> hashMapfirstElement = new HashMap<>();
        hashMapfirstElement.put("x", "val:23");
        hashMapfirstElement.put("str", "val:some String");
        firstElement.setParams(hashMapfirstElement);
        firstElement.setId("pojo.F1:f1");
        //When
        List<Node> list = graphCreator.createGraph();
        //Then
        Assert.assertEquals(firstElement,list.get(0));
    }

    @Test
    public void sortingTestAfterSortLastElementMustBeX(){
        //Given
        graphCreator = new GraphCreator(nodes);
        Node lastNode = new Node();
        HashMap<String, String> hashMapLastNode = new HashMap<>();
        hashMapLastNode.put("h1", "ref:h1");
        hashMapLastNode.put("h2", "ref:h2");
        hashMapLastNode.put("h3", "ref:h3");
        lastNode.setParams(hashMapLastNode);
        lastNode.setId("pojo.X:x");
        //When
        List<Node> list = graphCreator.createGraph();
        //Then
        Assert.assertEquals(lastNode,list.get(list.size()-1));
    }

   @Test(expected = CyclicalDependencyException.class)
    public void CyclicDependencyTest(){

        nodes.remove(2);
        Node f1Node = new Node();
        HashMap<String, String> hashMapF1 = new HashMap<>();
        hashMapF1.put("x", "val:23");
        hashMapF1.put("str", "val:some String");
        hashMapF1.put("h3", "ref:h3");
        f1Node.setParams(hashMapF1);
        f1Node.setId("pojo.F1:f1");
        nodes.add(f1Node);
        graphCreator = new GraphCreator(nodes);
        graphCreator.createGraph();
    }


}
