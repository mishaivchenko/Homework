package pojo;

public class H2{
   private G1 g1;
    private G2 g2;

    public H2(G1 g1, G2 g2) {
        this.g1 = g1;
        this.g2 = g2;
    }

    public G1 getG1() {
        return g1;
    }

    public void setG1(G1 g1) {
        this.g1 = g1;
    }

    public G2 getG2() {
        return g2;
    }

    public void setG2(G2 g2) {
        this.g2 = g2;
    }

}
