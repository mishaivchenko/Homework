package pojo;

public class X {
   private H1 h1;
    private H2 h2;
    private H3 h3;

    public X(H1 h1, H2 h2, H3 h3) {
        this.h1 = h1;
        this.h2 = h2;
        this.h3 = h3;
    }

    public H1 getH1() {
        return h1;
    }

    public void setH1(H1 h1) {
        this.h1 = h1;
    }

    public H2 getH2() {
        return h2;
    }

    public void setH2(H2 h2) {
        this.h2 = h2;
    }

    public H3 getH3() {
        return h3;
    }

    public void setH3(H3 h3) {
        this.h3 = h3;
    }

}
