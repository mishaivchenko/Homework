package pojo;

public class G2 {
    private F1 f1;

    public F1 getF1() {
        return f1;
    }

    public void setF1(F1 f1) {
        this.f1 = f1;
    }

    public G2(F1 f1) {
        this.f1 = f1;
    }
}
