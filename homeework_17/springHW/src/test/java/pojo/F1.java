package pojo;

public class F1 {
    private int x;
    private String str;

    public F1(int x, String str) {
        this.str =str;
        this.x = x;
    }

    public int getX() {
        return x;
    }

    public String getStr() {
        return str;
    }
}
