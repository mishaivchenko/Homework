package pojo;

public class H1 {
    private G1 g1;

    public H1(G1 g1) {
        this.g1 = g1;
    }

    public G1 getG1() {
        return g1;
    }

    public void setG1(G1 g1) {
        this.g1 = g1;
    }

}
