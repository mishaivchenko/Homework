package pojo;

public class H3 {
    private G2 g2;

    public H3(G2 g2) {
        this.g2 = g2;
    }

    public G2 getG2() {
        return g2;
    }

    public void setG2(G2 g2) {
        this.g2 = g2;
    }

}
