Calculator

1. This program simulates the work of the calculator. The program takes three arguments, two of which are numbers in double, and the third is a operator.

2. To build project execute in cmd: 


mvn clean install	

3. To start the program from console, you must enter two numbers and the desired operator.

4. Run the program only in Windows cmd or in the development environment. In the Bash console, the multiply and divide symbols - (*) and (/) do not work. 
At the command line windows, the symbol (*) is also reserved, so to perform the multiplication operation you must write the operator inside the quotation marks - "*".

5.To run application execute in cmd: 

// success scenario
 java -jar .\calc-console\target\calculate-jar-with-dependencies.jar 2 5 "*"

//fail scenario
 java -jar .\calc-console\target\calculate-jar-with-dependencies.jar