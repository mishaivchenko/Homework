package com.epam.rd.june2018.calc.console;

import com.epam.rd.june2018.calc.core.CalcImpl;

public class App {
    public static void main(String[] args) {


        if(args.length==3) {
        double number_1  = Double.parseDouble(args[0]);
        double number_2 = Double.parseDouble(args[1]);
        double result ;

        CalcImpl calc = new CalcImpl();

        switch (args[2]){
            case "+":
                result = calc.addition(number_1,number_2);
                break;
            case "-":
                result = calc.subtraction(number_1,number_2);
                break;
            case "*":
                result = calc.multiplication(number_1,number_2);
                break;
            case "/":
                result = calc.division(number_1,number_2);
                break;

                    default:
                        result=0;
                        System.out.println("Invalid input data");
        }

            System.out.println("number 1: " + args[0] +"\n" + "number 2: " + args[1] +"\n" + "operator: " + args[2] +"\n"+ "result: " + result);
        } else  {
            System.out.println("[] args is empty!");
        }
    }
}
