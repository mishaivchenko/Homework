package com.epm.recipe.persistence.in_memory.configuration;

import com.epm.recipe.persistence.RecipeRepository;
import com.epm.recipe.persistence.in_memory.InMemoryRecipeRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PersistenceInMemoryConfiguration {

    @Bean
    public RecipeRepository recipeRepository() {
        return new InMemoryRecipeRepository();
    }
}
