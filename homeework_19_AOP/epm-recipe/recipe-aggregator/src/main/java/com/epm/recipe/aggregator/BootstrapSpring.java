package com.epm.recipe.aggregator;

import com.epm.recipe.persistence.in_memory.configuration.PersistenceInMemoryConfiguration;
import com.epm.recipe.service.configuration.ServiceConfiguration;
import com.epm.recipe.web_ui.Ui;
import com.epm.recipe.web_ui.configuration.UiConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

public class BootstrapSpring {

    public static void main(String[] args) {
          ApplicationContext context = new ClassPathXmlApplicationContext(
                "/com/epm/recipe/web_ui/context.xml",
                "/com/epm/recipe/service/impl/context.xml",
                "/com/epm/recipe/persistence/in_memory/context.xml"
        );
        context.getBean(Ui.class).showRecipeOfTheDay();

        /*ApplicationContext context2 = new AnnotationConfigApplicationContext(
                PersistenceInMemoryConfiguration.class,
                ServiceConfiguration.class,
                UiConfiguration.class
        );

        context2.getBean(Ui.class).showRecipeOfTheDay("some String");*/
    }
}


