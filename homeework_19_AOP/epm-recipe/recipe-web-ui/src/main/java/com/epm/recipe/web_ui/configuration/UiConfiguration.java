package com.epm.recipe.web_ui.configuration;


import com.epm.recipe.service.RecipeService;
import com.epm.recipe.web_ui.Ui;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UiConfiguration {


    @Bean
    public Ui ui(RecipeService recipeService) {
        return new Ui(recipeService);
    }


}