package com.epm.recipe.service.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class TimerAspect {

    @Pointcut("@annotation(com.epm.recipe.service.aspect.AdviceRequired)")
    public void pointcut(){
    }

    @Around("pointcut()")
public Object around(ProceedingJoinPoint proceedingJoinPoint){
        long beforeMethod;
        long afterMethod;
        try {
            beforeMethod = System.nanoTime();
            Object o = proceedingJoinPoint.proceed();
            afterMethod = System.nanoTime();
            Object[] args = proceedingJoinPoint.getArgs();
            long leadTime = afterMethod - beforeMethod;
            System.out.println("The method worked " + leadTime + " nanoseconds");
            System.out.println("Method argument: " + args[0] + " " + args[0].getClass());
            return o;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return null;
    }
}
