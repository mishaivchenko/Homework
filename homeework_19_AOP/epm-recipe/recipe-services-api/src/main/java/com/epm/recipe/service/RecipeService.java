package com.epm.recipe.service;

import com.epm.recipe.domain.Recipe;

public interface RecipeService {
    Recipe recipeOfTheDay(String msg);
}
