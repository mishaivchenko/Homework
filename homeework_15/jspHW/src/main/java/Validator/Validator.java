package Validator;

public interface Validator<E> {
     boolean validate(E request);
}
