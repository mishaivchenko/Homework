package Controller;

import entity.User;
import service.serviceImpl.ServiceFactoryImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "loginController", urlPatterns = {"/Login"})
public class LogInController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    resp.sendRedirect("/login.jsp");
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       String username = req.getParameter("username");
       String password = req.getParameter("password");
        System.out.println(username);
        User userFromDb = ServiceFactoryImpl.getServiceFactory().getUserService().getUserByName(username);
        if(userFromDb!=null&&password.compareTo(userFromDb.getPassword())==0){
            req.getSession().setAttribute("User", userFromDb);
            req.getRequestDispatcher("/index.jsp").forward(req,resp);
        } else {
            req.setAttribute("errorLoginPassMessage","Неверный логин или пароль");
            req.getRequestDispatcher("/login.jsp").forward(req,resp);
        }
    }
}
