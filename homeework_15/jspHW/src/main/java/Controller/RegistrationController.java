package Controller;

import Validator.Validator;
import Validator.UserValidatorImpl;
import com.mysql.cj.api.Session;
import entity.User;
import service.serviceImpl.ServiceFactoryImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name="Registration", urlPatterns = "/registration")
public class RegistrationController extends HttpServlet {
    Validator<HttpServletRequest> userValidator = new UserValidatorImpl();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        String name = req.getParameter("name");
        String password = req.getParameter("password");
        String email = req.getParameter("email");
        String secondName = req.getParameter("secondName");
        String username = req.getParameter("username");

        if (userValidator.validate(req)) {
            User user = new User(1, name, secondName, username, password, email, User.Role.CLIENT, User.UserStatus.NONBLOCKING);
            if (ServiceFactoryImpl.getServiceFactory().getUserService().create(user)) {
                 req.setAttribute("username", user.getFirstName());
                 req.getRequestDispatcher("/index.jsp").forward(req,resp);
            } else {
                resp.sendRedirect("/error.jsp");
                }
        } else {
            req.setAttribute("errorLoginPassMessage", "validation error");
            req.getRequestDispatcher("/registration.jsp").forward(req, resp);
            }
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }
}
