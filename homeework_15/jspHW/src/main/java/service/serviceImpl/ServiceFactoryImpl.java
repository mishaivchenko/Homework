package service.serviceImpl;


import entity.User;
import service.Service;
import service.ServiceFactory;


public class ServiceFactoryImpl implements ServiceFactory<Service> {
    private static final ServiceFactoryImpl serviceFactory = new ServiceFactoryImpl();

    private Service<User> userService = new UserServiceImpl();



    public static ServiceFactoryImpl getServiceFactory(){
        return serviceFactory;
    }


    @Override
    public UserServiceImpl getUserService() {
        return (UserServiceImpl) this.userService;
    }
}
