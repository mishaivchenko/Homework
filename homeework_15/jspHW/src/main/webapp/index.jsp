<html>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<head>
    <meta charset="utf-8">
</head>

<body>
<c:if test="${User==null}">
<c:choose>
<c:when test="${requestScope.username != null}">
<ctg:hello name="${requestScope.username}"/>
</c:when>
   <c:otherwise>
       <h2>Hello, you at first page</h2>
   </c:otherwise>
</c:choose>
<ul>
    <li><a href="registration.jsp"><h3>go to registration page</h3></a></li>
</ul>
<ul>
    <li><a href="login.jsp"><h3>go to Login</h3></a></li>
</ul>
</c:if>
<c:if test="${User!=null}">
    <h1>Hello, ${User.getFirstName()}</h1>
    <h1>maybe you want <a href="/Logout">Logout?</a> </h1>
</c:if>
</body>
</html>
