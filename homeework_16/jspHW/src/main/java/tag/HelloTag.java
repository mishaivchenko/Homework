package tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import java.io.IOException;
@SuppressWarnings("serial")
public class HelloTag extends TagSupport {
    private String name;

    public void setName(String name) {
        this.name = name;
    }
    @Override
    public int doStartTag() throws JspException {
        try {
            String to =  name +  " added to database";
            pageContext.getOut().write("<hr/>" + to + "<hr/>");
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
        return SKIP_BODY;
    }
}

