package service;


import service.serviceImpl.UserServiceImpl;
public interface ServiceFactory<Service> {


    UserServiceImpl getUserService();
}
