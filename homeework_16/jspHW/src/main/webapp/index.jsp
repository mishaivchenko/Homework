<html>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<%@ taglib prefix="loc" uri="customtags" %>
<head>
    <meta charset="utf-8">
</head>

<body>
<jsp:include page="header.jsp"/>

<c:if test="${User==null}">
<c:choose>
<c:when test="${requestScope.username != null}">
<ctg:hello name="${requestScope.username}"/>
</c:when>
   <c:otherwise>
       <h2><loc:locale message="HelloYouAtFirstPage"/></h2>
   </c:otherwise>
</c:choose>
<ul>
    <li><a href="registration.jsp"><h3><loc:locale message="GoToRegistrationPage"/></h3></a></li>
</ul>
<ul>
    <li><a href="login.jsp"><h3><loc:locale message="GoToLogin"/></h3></a></li>
</ul>
</c:if>
<c:if test="${User!=null}">
    <h1><loc:locale message="Hello"/> ${User.getFirstName()}</h1>
    <h1><loc:locale message="MaybeYouWant"/> <a href="/Logout"><loc:locale message="Logout"/></a> </h1>
</c:if>
</body>
</html>
