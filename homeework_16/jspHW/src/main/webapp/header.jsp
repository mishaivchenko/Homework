<%@ taglib prefix="loc" uri="customtags" %>

<%--
  Created by IntelliJ IDEA.
  User: Misha
  Date: 04.09.2018
  Time: 21:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<form class="form-group">
    <select class="custom-select" name = "lang" required>
        <option value="ru">Russian</option>
        <option value="en">English</option>
        <option selected ="selected"></option>
    </select>
    <button class="btn btn-link my-2 my-sm-0" type="submit" formaction="/changeLocal" formmethod="get"><loc:locale message="language"/></button>
    <%--<loc:locale message="LogoutButton"/>--%>
</form>
</html>
