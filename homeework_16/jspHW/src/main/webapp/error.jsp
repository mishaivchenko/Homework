<%--
  Created by IntelliJ IDEA.
  User: Misha
  Date: 28.08.2018
  Time: 21:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="loc" uri="customtags" %>

<html>
<head>
    <meta charset="utf-8">
    <title><loc:locale message="errorPage"/></title>
</head>
<body>
<jsp:include page="header.jsp"/>
    <h1><loc:locale message="ThisUserAlreadYExistInDatabase"/></h1>
</body>
</html>
