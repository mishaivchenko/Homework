<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Misha
  Date: 31.08.2018
  Time: 14:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
</head>
<body>
<jsp:include page="header.jsp"/>
<div class="container">
    <form >
        <p><input placeholder= "username" type="text" name = "username" required><c:out value="Enter your username"/></p>
        <p><input placeholder= "password" type="password" name = "password" required><c:out value="Enter your password"/></p>
        <p><input type = "submit" value = "Log in" formaction = "/Login" formmethod = "post"></p>
        <br/>
        ${errorLoginPassMessage}
        <br/>
    </form>

</div>
</body>
</html>
