
<%--
  Created by IntelliJ IDEA.
  User: Misha
  Date: 28.08.2018
  Time: 19:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="loc" uri="customtags" %>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><loc:locale message="Registration"/></title>
</head>
<body>
<jsp:include page="header.jsp"/>
<div class="container">
    <form >
        <p><input placeholder= "name"  name = "name" required><loc:locale message="EnterYourName"/></p>
        <p><input placeholder= "second name" name = "secondName" required><loc:locale message="EnterYourSurname"/></p>
        <p><input placeholder= "email" type="email" name = "email" required><loc:locale message="EnterYourEmail"/></p>
        <p><input placeholder= "password" type="password" name = "password" required><loc:locale message="EnterYourPassword"/></p>
        <p><input placeholder= "username" type="text" name = "username" required><loc:locale message="EnterYourUsername"/></p>
        <p><input type = "submit" value = "add User" formaction = "/registration" formmethod = "post"></p>

    </form>

</div>
${requestScope.errorLoginPassMessage}
</body>
</html>
