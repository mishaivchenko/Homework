1. This program is the creation of collections such as ArrayList and LinkedList.
2. For the work of collections with their own data types, you need to override the ToString method () for these types;
3. In both collections the numbering of elements starts from zero
Collection methods MyArrayList and MyLinkedList
1. MyArrayList
	1.1 public MyArrayList()  -  Initializes the class object with the default capacity. the default capacity is 16.
   	1.2 public MyArrayList(int size) - Initialize the class object with capacity = size. If size < 0 throw IllegalArgumentException()
   	1.3 public void updaty(int index, E element) - update element by index. If the index is not correct throw IllegalArgumentException()
	1.4 public E get(int index) - get element by index. If the index is not correct throw IllegalArgumentException()
	1.5 public void set(int index, E element) - set element by index. If the index is not correct throw IllegalArgumentException()
	1.6 public boolean add(E element) - adds an element to the next cell, if the array is finished, creates a new enlarged array and copies the elements into it.
	1.7 public void delete(int index) - delete element by index and copy all element in new array  
	1.8 public void printList() - Outputs array elements to the console
	1.9 public E remove(int index) - removes and returns an item by index
	1.10 public int capacity() - return current capacity
	1.11 public int getSize() - return current size
2. MyLinkedList
	2.1 public MyLinkedList() - initializes the class object with 2 nodes(first and last).
	2.2 public MyLinkedList(int size) initializes the class object with (size) nodes. 
	2.3 public E get() - get element by index. If the index is not correct throw IllegalArgumentException()
	2.4 public void set(int index, E element) - set element by index. If the index is not correct throw IllegalArgumentException()
	2.5 public boolean add(E element) - adds an element to the last position.
	2.6 public E remove(int index) -  removes and returns an item by index. 
	2.7 public void add(int index, E element) - adds an element to the specified position and shifts all the following elements to the right
	2.8 public void printList() - outputs array elements to the console
	2.9 public void delete(int index) - delete element by index.
	2.10 public int getSize() - return current size
	2.11 public void addFirts(E element) - add element in first position
	2.12 public void addLast(E element) - add element in last position
	2.13 public E removeFirst() - removes and returns first element
	2.14 public E removeLast() - removes and returns last element
	2.15 public E getFirst - get first element
	2.16 public E getLast - get last element
