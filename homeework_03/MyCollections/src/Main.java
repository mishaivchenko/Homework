import ImplClass.MyArrayList;
import ImplClass.MyLinkedList;
import POJO.Car;



public class Main {
    public static void main(String[] args) {

/////////////////////**ArrayList**////////////////////////
        MyArrayList<Car> myArrayList = new MyArrayList();
        System.out.println("size: " + myArrayList.getSize());
        myArrayList.add(new Car("BMW",4));
        myArrayList.add(new Car("Mercedes",2));
        myArrayList.add(new Car("Audi",3));
        myArrayList.add(new Car("jiguli",1));


        myArrayList.printList();
        System.out.println("size: " + myArrayList.getSize());
        System.out.println("capacity" + myArrayList.capacity());
        System.out.println(myArrayList.get(2) + " get element ");

        myArrayList.update(2, new Car("volga",3));

        System.out.println(myArrayList.get(2) + " after update");

        myArrayList.delete(2);

        System.out.println("list after delete:\n");
        myArrayList.printList();
        System.out.println(myArrayList.remove(1) + " \nreturn element after remove");
        System.out.println("list after remove");
        myArrayList.printList();


        myArrayList.set(0,new Car("citroen",2));
        System.out.println(myArrayList.get(0)+ " \nelement after set\n");


        MyArrayList<Car> myArrayListWithInitialSize = new MyArrayList<Car>(10);
        System.out.println("capacity:" + myArrayListWithInitialSize.capacity());
        System.out.println("size: "  + myArrayListWithInitialSize.getSize() );


///////////////////////////LinkedList//////////////////////
        MyLinkedList<Car> myLinkedList = new MyLinkedList<>();
        myLinkedList.add(new Car("niva",2));
        System.out.println("\n" + myLinkedList.getSize());
        myLinkedList.printList();
        System.out.println();
        myLinkedList.add(new Car("Kamaz",22));
        myLinkedList.printList();
        System.out.println(myLinkedList.getSize());

        myLinkedList.addFirts(new Car("sodios",11));
        System.out.println();
        myLinkedList.add(0,new Car("sodios",12));
        myLinkedList.printList();
        System.out.println();
        myLinkedList.addLast(new Car("ziipeer",0));
        myLinkedList.printList();
        System.out.println("\n" +myLinkedList.getSize());
        myLinkedList.add(5,new Car("BatMobile",2));

        myLinkedList.printList();
        System.out.println(myLinkedList.getSize());
        System.out.println(myLinkedList.get(3) + "\n get element+\n");

        System.out.println(myLinkedList.remove(3) + " remove element");
        System.out.println("List after remove");
        myLinkedList.printList();
        myLinkedList.set(3,new Car("jugguli 2", 0));

        System.out.println("\nlist after set");
        myLinkedList.printList();
        System.out.println();
        System.out.println(myLinkedList.remove(0) + " remove element");
        System.out.println( "\nlist after remove ");
        myLinkedList.printList();

        MyLinkedList<Car> myLinkedListWithInitialSize = new MyLinkedList<>(10);
        System.out.println("\n" + myLinkedListWithInitialSize.getSize());
        myLinkedListWithInitialSize.addFirts(new Car("dog",0));
        myLinkedListWithInitialSize.addFirts(new Car("dog",0));
        myLinkedListWithInitialSize.printList();
        System.out.println(myArrayListWithInitialSize.getSize());
        myLinkedListWithInitialSize.set(9,new Car("cat",2));
        myLinkedListWithInitialSize.printList();
        System.out.println(myLinkedListWithInitialSize.getSize());


    }

}

