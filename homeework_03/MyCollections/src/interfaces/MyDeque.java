package interfaces;

import java.util.Collection;

public interface MyDeque<E> extends MyCollection<E> {
    void addFirts(E element);

    void addLast(E element);

    E removeFirst();

    E removeLast();

    E getFirst();

    E getLast();

}
