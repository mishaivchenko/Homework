This program is designed for parsing logs with the ability to save to a file. 
The program works with a command line using the args4j library. After mvn clean install two files with the extension .jar will appear.

To run the application, you need to use homeworkWithManifest.jar


Launch Examples:
1) java -jar homeworkWithManifest.jar -file C:\Users\Misha\IdeaProjects\RegexpSerialHW\src\logs.txt         ----> Parsing logs 
2) java -jar homeworkWithManifest.jar -file C:\Users\Misha\IdeaProjects\RegexpSerialHW\src\logs.txt -view 5   -----> Log parsing and output to the console 5 logs from each module
3) java -jar homeworkWithManifest.jar -file java -jar homeworkWithManifest.jar -file C:\Users\Misha\IdeaProjects\RegexpSerialHW\src\logs.txt -view 5 -save H:\ZAR\logs.dat
 -------> point two plus serialization of a List with logs to a file (If you use the -save flag, you must specify the full path + filename)
4) java -jar homeworkWithManifest.jar  -load H:\ZAR\logs.dat ----->Loading an Existing Serialized File with Logs (If you use the -load flag, you must specify the full path + filename)
