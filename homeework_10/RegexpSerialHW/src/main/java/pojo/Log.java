package pojo;

public class Log {
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getOperationName() {
        return operationName;
    }

    public void setOperationName(String operationName) {
        this.operationName = operationName;
    }

    public Long getExecutionTime() {
        return executionTime;
    }

    public void setExecutionTime(Long executionTime) {
        this.executionTime = executionTime;
    }

    private String date;
    private String moduleName;
    private String operationName;
    private Long executionTime;

    public Log(String date,String moduleName,String operationName,Long executionTime){
        this.date = date;
        this.moduleName = moduleName;
        this.operationName = operationName;
        this.executionTime = executionTime;
    }
}
