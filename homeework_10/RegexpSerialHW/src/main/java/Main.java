
import healper.CheckFileHealper;
import impl.LogParserImpl;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import settings.Settings;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.TreeMap;

public class Main {
    public static void main(String[] args) throws IOException {
        CheckFileHealper checkFileHealper = new CheckFileHealper();
        Settings settings = new Settings();
        CmdLineParser cmdLineParser = new CmdLineParser(settings);

        try {
            cmdLineParser.parseArgument(args);
            System.out.println("settings " + settings);
            LogParserImpl LogParserImpl = new LogParserImpl();
            List<String> list;

            if(settings.getPathToSavedFile()!=null&&settings.getPathToSavedFile().length()>0&&checkFileHealper.checkFile(settings.getPathToSavedFile())) {
                list = LogParserImpl.deSerializeList(settings.getPathToSavedFile());
                for (String s: list) {
                    System.out.println(s);
                }
            }

            if (settings.getPathToFile()!=null && settings.getPathToFile().length()>0 && checkFileHealper.checkFile(settings.getPathToFile()) && settings.getAmount()>0){
                list = LogParserImpl.getLogs(Paths.get(settings.getPathToFile()),settings.getAmount());
                for (String s:list) {
                    System.out.println(s);
                }
                if(settings.getSaveLocation()!=null&&settings.getSaveLocation().length()>0) {
                    LogParserImpl.serializeList(settings.getSaveLocation(), list);
                }
            }
        } catch (CmdLineException e) {
            e.printStackTrace();
                cmdLineParser.printUsage(System.out);
        }
    }
}
