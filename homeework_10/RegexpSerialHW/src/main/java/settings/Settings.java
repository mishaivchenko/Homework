package settings;

import org.kohsuke.args4j.Option;

public class Settings {


    @Option(name = "-load",required = false, usage = "set saved file location")
    private String pathToSavedFile;
    @Option(name = "-file",required = false, usage = "set file location")
     private String pathToFile;
    @Option(name="-view", required = false, usage = "set Amount")
     private int amount;
    @Option(name="-save", required = false, usage = "set save location")
    private String saveLocation;

    public String getSaveLocation() {
        return saveLocation;
    }

    public String getPathToSavedFile() {
        return pathToSavedFile;
    }

    public String getPathToFile() {
        return pathToFile;
    }

    public int getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return "Settings{" +
                "pathToSavedFile='" + pathToSavedFile + '\'' +
                ", pathToFile='" + pathToFile + '\'' +
                ", amount=" + amount +
                ", saveLocation='" + saveLocation + '\'' +
                '}';
    }
}
