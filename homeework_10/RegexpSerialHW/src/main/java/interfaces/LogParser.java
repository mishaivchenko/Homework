package interfaces;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public interface LogParser {
   List<String> getLogs(Path path,int amount) throws IOException;
   //void viewTopLogs();
}
