package impl;//2018-07-30 11:49:27.661 INFO Module=MIC Operation: Logout Execution time: 17281 ms



import pojo.Log;
import interfaces.LogParser;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LogParserImpl implements LogParser {



    final private Pattern p = Pattern.compile("(\\d{4}-\\d{2}-\\d{2}\\s+\\d{2}:\\d{2}:\\d{2}\\.\\d{3})" + // Date дни ++ часы
            "(\\s+[A-Z]+\\s+Module=)" +
            "(\\w{3})" + // Модуль
            "(\\s+Operation:\\s+)" +
            "(\\w+\\s*\\w*)" +//операция
            "(\\s+Execution time:\\s*)" +
            "(\\d{3,})");//время



   public void serializeList(String name,List<String> list){
       System.out.println(name);
       File file = new File(name);
       System.out.println(file.getName());
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream( file.getPath()))){
            oos.writeObject(list);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public  List<String> deSerializeList(String name){
        List<String> list2 = null;
        try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(name)))
        {
            list2 = (List<String>) ois.readObject();
        }
        catch(Exception ex){
            System.out.println(ex.getMessage());
        }
        if(list2==null)throw new IllegalArgumentException();
        else return list2;
    }

   private List<String> getTopLogs(TreeMap<String,List<Log>> treeMap, int amount){
        List<String> list = new ArrayList<>();
        for (String s : treeMap.keySet()){
            List<Log> logList = treeMap.get(s);
            list.add(s.toUpperCase()+ " Module");
            for (int i=0;i<amount;i++){
                list.add("Operation "+logList.get(i).getOperationName()+" " + logList.get(i).getExecutionTime()+" ms, "+ "finished at " +logList.get(i).getDate());
            }
        }
        return list;
    }

    private  SortedMap<String,List<Log>> listToSortedMap(List<Log> list){
        Map<String, List<Log>> list2 =  list.stream()
                .sorted(Comparator.comparing(Log::getExecutionTime).reversed())
                .collect(Collectors.groupingBy(Log::getModuleName,Collectors.toList()));
        return new TreeMap<>(list2);
    }

    private  List<Log> logToList(Path logFile) throws IOException {
        try (Stream <String> stream = Files.lines(logFile)){
             return stream.map(p::matcher).flatMap(m->{
                List<Log> list = new ArrayList<>();
                while (m.find()){
                    list.add(new Log(m.group(1),m.group(3),m.group(5),Long.parseLong(m.group(7))));
                }
                return list.stream();
            }).collect(Collectors.toList());
        }
    }


    @Override
    public List<String> getLogs(Path path, int amount) throws IOException {
        List<Log> list = logToList(path);
        TreeMap<String,List<Log>> treeMap = (TreeMap<String, List<Log>>) listToSortedMap(list);
        return getTopLogs(treeMap,amount);
    }
}
