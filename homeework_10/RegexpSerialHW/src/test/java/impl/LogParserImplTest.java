package impl;

import interfaces.LogParser;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import pojo.Log;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.Assert.*;

public class LogParserImplTest {


    @Test
    public void serializeListCreateFileTest() throws IOException {
        //Given
        File file = null;
        LogParser logParser = new LogParserImpl();
        //When
        List listWithLogs = logParser.getLogs(Paths.get("src" + File.separator + "logs.txt"),5);
        ((LogParserImpl) logParser).serializeList("src"+File.separator+ "serializedFile.dat",listWithLogs);
            file = new File( "src"+ File.separator+ "serializedFile.dat");
        //Then
        Assert.assertEquals(true,file.exists());
        file.delete();
    }

    @Test
    public void deSerializeListTestAfterTestDeserializeListMustBeEqualToListBefore() throws IOException {

        //Given
        LogParser logParser = new LogParserImpl();
        //When
        List listWithLogs = logParser.getLogs(Paths.get("src" + File.separator + "logs.txt"),5);
        ((LogParserImpl) logParser).serializeList("src"+File.separator+ "serializedFile.dat",listWithLogs);
        List afterDeserialize = ((LogParserImpl) logParser).deSerializeList("src"+File.separator+ "serializedFile.dat");
        //Then
        Assert.assertEquals(listWithLogs,afterDeserialize);
        File file = new File( "src"+ File.separator+ "serializedFile.dat");
        file.delete();
    }

    @Test
    public void getLogsTestMustReturnListSizeOfListMustBeEqualTen() throws IOException {
        //Given
        int expectedLength = 36; //30 Logs and 6 name of Modules
        LogParser logParser = new LogParserImpl();
        //When
        List listWithLogs = logParser.getLogs(Paths.get("src" + File.separator + "logs.txt"),5);
        //Then
        Assert.assertEquals(expectedLength,listWithLogs.size());
    }
    @Test
    public void getLogsTestConsistencyCheck() throws IOException {
        //Given
        String moduleASC;
        String moduleCMN ;
        LogParser logParser = new LogParserImpl();
        //When
        List<String> listWithLogs = logParser.getLogs(Paths.get("src" + File.separator + "logs.txt"),5);
        moduleASC = listWithLogs.get(0);
        moduleCMN = listWithLogs.get(6);
        //Then
        Assert.assertTrue(moduleASC.compareTo(moduleCMN)<0);
    }



}