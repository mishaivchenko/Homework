package interfaces;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface Unzipper {
    public void doUnzip() throws IOException;
}
