package interfaces;

import java.io.File;
import java.io.IOException;
import java.util.zip.ZipOutputStream;

public interface Zipper {

    void doZip(File files) throws IOException;
}
