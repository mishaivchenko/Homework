package Impl;

import interfaces.Zipper;

import java.io.*;
import java.util.Scanner;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipperImpl implements Zipper {

    private String zipName;
    private Scanner s = new Scanner(System.in);
    private ZipOutputStream out;
    private int parent;
    private File file;

    public ZipperImpl() {

    }

    public ZipperImpl(String zipName) {

        this.zipName = zipName;
    }


    @Override
    public void doZip(File files) throws IOException {
        this.file = files;
        if (checkForTheSourceFile()) {

            parent = files.getParent().length() + 1;
            if (zipName == null) this.zipName = files.getPath() + ".zip";
            if (!checkingForAFile()) {
                try (ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(zipName))) {
                    zip(files, zipOutputStream);
                }

            } else if (checkingForAFile() && chooseOfAction()) {
                try (ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(zipName))) {
                    zip(files, zipOutputStream);
                }
            }
        } else throw new FileNotFoundException();
    }


    private void zip(File files, ZipOutputStream zout) throws IOException {
        for (File file : files.listFiles()) {
            if (file.isDirectory()) {
                zip(file, zout);
            } else {
                zout.putNextEntry(new ZipEntry(file.getPath().substring(parent)));
                write(new FileInputStream(file), zout);
            }
        }
    }

    private boolean chooseOfAction() {
        System.out.println("\n" +
                "An archive with this name already exists. If you want to overwrite the archive, enter 'y'.\n" +
                "If you want to leave the previous version, enter 'n' ");
        String var = s.nextLine();
        if (var.compareTo("y") == 0) return true;
        else if (var.compareTo("n") == 0) return false;
        else chooseOfAction();
        return false;
    }

    private static void write(FileInputStream fis, ZipOutputStream zipOutputStream) throws IOException {
        byte[] buffer = new byte[fis.available()];
        fis.read(buffer); // считываем содержимое файла в массив byte
        zipOutputStream.write(buffer); // добавляем содержимое к архиву
        zipOutputStream.closeEntry();
        fis.close();
    }

    private boolean checkForTheSourceFile() {
        if (file != null) {
            return file.exists();
        } else return false;
    }

    private boolean checkingForAFile() {
        return new File(zipName).exists();
    }

}