package Impl;

import interfaces.Unzipper;

import java.io.*;
import java.util.Scanner;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class UnZipperImpl implements Unzipper {

    Scanner s = new Scanner(System.in);
    private String zipName;
    private String dstDirectory;

    public UnZipperImpl(String zipName, String dstDirectory) {
        this.zipName = zipName;
        this.dstDirectory = dstDirectory;
    }

    public UnZipperImpl(String zipName) {
        this.zipName = zipName;
    }


   public void doUnzip() throws FileNotFoundException {
        if (checkForTheSourceFile()) {
            checkDirectory();
            if (!checkingForAFile()) {
                unZip(zipName);
            } else if (checkingForAFile() && chooseOfAction()) {
                unZip(zipName);
            }
        } else throw new FileNotFoundException();
    }

    private void unZip(String zipName) {
         createIfNotExist();
        try {
            ZipInputStream zis = new ZipInputStream(new FileInputStream(zipName));
            ZipEntry ze = zis.getNextEntry();
            String nextFileName;
            while (ze != null) {
                nextFileName = ze.getName();
                File nextFile = new File(dstDirectory + File.separator
                        + nextFileName);
                if (ze.isDirectory()) {
                    nextFile.mkdir();
                } else {

                    new File(nextFile.getParent()).mkdirs();

                    try (FileOutputStream fos = new FileOutputStream(nextFile)) {
                      write(fos,zis);
                    }
                }
                ze = zis.getNextEntry();
            }
            zis.closeEntry();
            zis.close();
        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        }
    }

    private boolean chooseOfAction() {
        System.out.println("\n" +
                "An folder with this name already exists. If you want to overwrite the folder, enter 'y'.\n" +
                "If you want to leave the previous version, enter 'n' ");
        String var = s.nextLine();
        if (var.compareTo("y") == 0) return true;
        else if (var.compareTo("n") == 0) return false;
        else chooseOfAction();
        return false;
    }

    private boolean checkingForAFile() {
        return new File(dstDirectory).exists();
}

    private File createIfNotExist() {
        File dstDir = new File(dstDirectory);
        if (!dstDir.exists()) {
            dstDir.mkdir();
        }
        return dstDir;
    }

    private void write(FileOutputStream fos,ZipInputStream zis) throws IOException {
        byte[] buffer = new byte[zis.available()];
        zis.read(buffer);
        fos.write(buffer);
    }

    private boolean checkForTheSourceFile() {
        if (zipName != null) {
            return new File(zipName).exists();
        } else return false;
    }

    private void checkDirectory(){
        String name = new File(zipName).getName();
        if (dstDirectory == null) {
            dstDirectory = destinationDirectory(zipName);

        } else dstDirectory = new File(dstDirectory + File.separator+name.substring(0,name.lastIndexOf("."))).getPath();
    }
    private String destinationDirectory(String srcZip) {
        return srcZip.substring(0, srcZip.lastIndexOf("."));
    }
}
