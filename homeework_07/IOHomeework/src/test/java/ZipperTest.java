import Impl.ZipperImpl;
import interfaces.Zipper;
import org.junit.*;
import org.junit.rules.ExpectedException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ZipperTest {
    String zipName;
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    @After
    public void deleteZip(){
        File file = new File(zipName);
        if(file.exists()) file.delete();
    }

    @Test
    public void doZipTestMustCreateZipAndReturnTrue() throws IOException {
        //Given
       zipName = "src" +File.separator + "someFile.zip";
        Zipper zipper = new ZipperImpl(zipName);
        File file = new File("src"+ File.separator+ "folder") ;
        //When
        zipper.doZip(file);
        // Then
        Assert.assertEquals(true,new File(zipName).exists());

    }
    @Test
    public void doZipTesMustCreateAZipWithANameEqualToTheSpecifiedInTheConstructor() throws IOException {
        //Given
        String expectedName = "otherFile.zip";
        zipName = "src" +File.separator + "otherFile.zip";
        Zipper zipper = new ZipperImpl(zipName);
        File file = new File("src"+ File.separator+ "folder") ;
        //When
        zipper.doZip(file);
        //Then
         String actualName = new File(zipName).getName();
         Assert.assertEquals(expectedName,actualName);
         new File(zipName).delete();
    }
    @Test
    public void testConstructorWithoutParametersMustCreateZipInCurrentDirectoryAndReturnTrue() throws IOException {
        //Given
        Zipper zipper = new ZipperImpl();
        File file = new File("src"+ File.separator+ "folder") ;
        //When
        zipper.doZip(file);
        //Then
        File expectedFile = new File(file.getPath()+".zip");
        zipName = expectedFile.getPath();
        Assert.assertEquals(true,expectedFile.exists());
    }
    @Test
    public void testConstructorWithoutParametersMustCreateZipWithNameEqualToZipName() throws IOException {
        //Given
        String expectedName = "folder.zip";
        Zipper zipper = new ZipperImpl();
        File file = new File("src"+ File.separator+ "folder") ;
        //When
        zipper.doZip(file);
        //Then
        File actualFile = new File(file.getPath()+".zip");
        zipName = actualFile.getPath();
        Assert.assertEquals(expectedName,actualFile.getName());
    }


    @Test
    public void tryToZipNonExistentFile() throws IOException {
        expectedException.expect(FileNotFoundException.class);
        zipName = "src" +File.separator + "someFile.zip";
        Zipper zipper = new ZipperImpl(zipName);
        zipper.doZip(null);
    }
}
