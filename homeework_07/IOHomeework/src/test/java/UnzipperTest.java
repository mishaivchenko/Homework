import Impl.UnZipperImpl;
import interfaces.Unzipper;
import org.junit.After;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class UnzipperTest {
    String zipName;
    String folderName;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    @After
    public void deleteFolder(){
        File file = new File(folderName);
        if(file.exists()) {
            deleteHelper(file);
            file.delete();
        }
    }

    @Test
    public void doUnZipTestMustCreateFolderAndReturnTrue() throws IOException {
        //Given
        String expectedFolder = "src" + File.separator + "zipFolder";
        zipName = "src" +File.separator + "zipFolder.zip";
        folderName = "src"+ File.separator;
        Unzipper unzipper = new UnZipperImpl(zipName,folderName);
        //When
        unzipper.doUnzip();
        //Then
        folderName = expectedFolder;
        Assert.assertEquals(true,new File(expectedFolder).exists());
    }
    @Test
    public void doUnZipTesMustCreateFolderWithNameEqualToTheSpecifiedInTheConstructor() throws IOException{
        //Given
        String expectedName = "zipFolder";
        zipName = "src" +File.separator + "zipFolder.zip";
        folderName = "src"+ File.separator;
        Unzipper unzipper = new UnZipperImpl(zipName,folderName);
        //When
        unzipper.doUnzip();
        //Then
        folderName = "src" + File.separator + "zipFolder";
        Assert.assertEquals(expectedName,new File(folderName).getName());
    }
    @Test
    public void testConstructorWithOneParameterMustCreateFolderInCurrentDirectoryAndReturnTrue() throws IOException {
        String expectedFolder = "src" + File.separator + "zipFolder";
        zipName = "src" +File.separator + "zipFolder.zip";
        Unzipper unzipper = new UnZipperImpl(zipName);
        //When
        unzipper.doUnzip();
        //Then
        folderName = expectedFolder;
        Assert.assertEquals(true,new File(expectedFolder).exists());
    }

    @Test
    public void testConstructorWithOneParameterMustCreateZipWithNameEqualToZipName() throws IOException{
        //Given
        String expectedName = "zipFolder";
        zipName = "src" +File.separator + "zipFolder.zip";
        Unzipper unzipper = new UnZipperImpl(zipName);
        //When
        unzipper.doUnzip();
        //Then
        folderName = "src" + File.separator + "zipFolder";
        Assert.assertEquals(expectedName,new File(folderName).getName());
    }
    @Test
    public void tryToUnzipNonExistentFile() throws IOException {
        expectedException.expect(FileNotFoundException.class);
        zipName = "src" +File.separator + "someFolder.zip";
        Unzipper unzipper = new UnZipperImpl(zipName);
        folderName = "src" + File.separator + "zipFolder";
        unzipper.doUnzip();
    }




    private void deleteHelper(File file){
        for (File fileToDelete:file.listFiles()) {
            if(fileToDelete.isDirectory()){
                deleteHelper(fileToDelete);
            }
            else {
                fileToDelete.delete();
            }
            file.delete();
        }
    }
}
