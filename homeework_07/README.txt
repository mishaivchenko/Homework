
The program simulates the work of Zip archiver. There are two classes of ZipperImpl and UnzipperImpl for implementing the Zipper and Unzipper interfaces.

Several options are possible. When creating an object that implements the Zipper interface, if you send a directory to the constructor for the unpacking and the name of the Zip archive - the archive will be unpacked into the specified directory.
	String filename = "H:\\Copy\\folder";
      	 String zipName = "H:\\otherFile.zip";
       	 Zipper zipper = new ZipperImpl(zipName);
	 File file = new File(filename);
	 zipper.doZip(file);
 If you use the constructor without parameters, the archive will be created in the current directory.
	 String filename = "H:\\Copy\\folder";
	 Zipper zipper = new ZipperImpl();
	 File file = new File(filename);
        	 zipper.doZip(file);

When working with the Unzipper object, if you specify only the name of the zip archive in the designer, the archive will be parsed into the current directory.
	 Unzipper unzipper = new UnZipperImpl(zipName);
            	unzipper.doUnzip();

If you pass two arguments to the method, the archive is parsed into the specified directory
	Unzipper unzipper = new UnZipperImpl(zipName,folderName);
            	unzipper.doUnzip();


If the zip file or folder already exists, you must confirm the action:
'y' - consent to overwrite
'n' - leave the previous version
If file not exist program throws FileNotFoundException