package com.epm.recipe.security.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class ApplicationConfig {


    @Bean
    public ApplicationSecurityConfig applicationSecurityConfig() {
        ApplicationSecurityConfig applicationConfig = new ApplicationSecurityConfig(logingAccessDeniedHandler());
        return applicationConfig;
    }

    @Bean
    public LogingAccessDeniedHandler logingAccessDeniedHandler() {
        return new LogingAccessDeniedHandler();
    }

}
