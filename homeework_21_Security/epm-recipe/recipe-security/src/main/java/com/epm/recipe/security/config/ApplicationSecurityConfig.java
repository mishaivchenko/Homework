package com.epm.recipe.security.config;

import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;


@EnableWebSecurity
public class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter {

    private LogingAccessDeniedHandler logingAccessDeniedHandler;

    public ApplicationSecurityConfig(LogingAccessDeniedHandler logingAccessDeniedHandler) {
        this.logingAccessDeniedHandler = logingAccessDeniedHandler;
    }


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication().passwordEncoder(NoOpPasswordEncoder.getInstance())
                .withUser("user").password("user").roles("USER").and()
                .withUser("admin").password("admin").roles("ADMIN").and()
                .withUser("customer").password("customer").roles("CUSTOMER");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/deleteRecipe**").access("hasRole('ROLE_ADMIN') or hasRole('ROLE_CUSTOMER')")
                .antMatchers("/createNewRecipe**","/updateRecipe**").access("hasRole('ROLE_CUSTOMER')")
                .antMatchers("/getAllRecipes**","/recipeById/**").access("hasRole('ROLE_USER')")
                .and()
                .formLogin()
                .and()
                .exceptionHandling()
                .accessDeniedHandler(logingAccessDeniedHandler)
                .and()
                .logout()
                .invalidateHttpSession(true)
                .clearAuthentication(true)
                .permitAll();
    }
}