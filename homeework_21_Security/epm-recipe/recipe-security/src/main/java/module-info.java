module recipe.security {
    requires spring.context;
    requires spring.aop;
    opens com.epm.recipe.security.config to spring.core;
    requires spring.security.core;
    requires spring.security.config;
    requires spring.security.web;
    requires javax.servlet.api;
    requires spring.beans;
    exports com.epm.recipe.security.config;
}