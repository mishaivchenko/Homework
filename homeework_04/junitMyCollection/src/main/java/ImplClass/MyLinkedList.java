package ImplClass;

import interfaces.MyDeque;
import interfaces.MyList;


/*Связной список
        вставка элемента в начало, середину, конец списка
        удаление элемента из начала, середины, конца списка
        (удаление возвращает удаляемый элемент, при этом между элементами не должно быть пробелов)
        получение первого, последнего элемента
        получение произвольного элемента по индексу*/


public class MyLinkedList<E> implements MyList<E> , MyDeque<E> {
    private MyNode<E> first;
    private MyNode<E> last;
    private int size;


    public MyLinkedList() {
        first = new MyNode(null, last, null);
        last = new MyNode(null, null, first);
        size = 0;
    }

    public MyLinkedList(int initialSize){
        first = new MyNode(null, last, null);
        last = new MyNode(null, null, first);
        size = 0;
            for(int i = 0;i<initialSize;i++){
                addLast(null);
            }

    }


    private class MyNode<E> {
        private E current;
        private MyNode<E> next;
        private MyNode<E> previous;


        public MyNode(E current, MyNode<E> next, MyNode<E> previous) {
            this.current = current;
            this.next = next;
            this.previous = previous;
        }

        public E getCurrent() {
            return this.current;
        }

        public void setCurrent(E current) {
            this.current = current;
        }

        public MyNode<E> getNext() {
            return this.next;
        }

        public void setNext(MyNode<E> next) {
            this.next = next;
        }

        public void setPrevious(MyNode<E> previous) {
            this.previous = previous;
        }

        public MyNode<E> getPrevious() {
            return this.previous;
        }
    }

//Get Element by index
    @Override
    public E get(int index) {
        checkIndex(index);
        MyNode<E> currentNode = first;
        for (int i = 0; i < index; i++) {
            currentNode = currentNode.getNext();
        }         return currentNode.getCurrent();
    }
// Set element by index
    @Override
    public void set(int index, E element) {
        checkIndex(index);
        if (size != 0) {
            if (index == 0) {
                first.setCurrent(element);
            } else if (index == size) {
                last.setCurrent(element);
            } else {
                MyNode<E> currentNode = first;
                for (int i = 0; i < index; i++) {
                    currentNode = currentNode.getNext();
                }
                currentNode.setCurrent(element);
            }


        } else throw new IllegalArgumentException();
    }


/*Add element in Linked list
    if list is empty add like first element
    else add like last
  */
    @Override
    public boolean add(E element) {
        if (size == 0) {
            initializeLink(element);
            return true;
        } else {
            addLast(element);
            return true;
        }
    }
    // Delete element by index
    @Override
    public void delete(int index) {
        checkIndex(index);
            if(index==0){
                removeFirst();
            } else if(index==size){
                removeLast();
            } else {
                MyNode<E> currentNode = first;
                for (int i = 0; i < index; i++) {
                    currentNode = currentNode.getNext();
                }
                  currentNode.getPrevious().setNext(currentNode.getNext());
                    currentNode.getNext().setPrevious(currentNode.getPrevious());
                        size--;
            }

    }
        //Add element by index
    public void add(int index,E element) {
        checkIndex(index);
        if (index==0) {
            addFirts(element);
        } else if (index == size) {
            addLast(element);
        } else {
            MyNode<E> currentNode = first;
            for (int i = 0; i < index; i++) {
                currentNode = currentNode.getNext();
            }
            MyNode addNode = new MyNode(element, currentNode, currentNode.getPrevious());
            currentNode.setPrevious(addNode);
            currentNode.getPrevious().getPrevious().setNext(addNode);
            size++;
        }
    }


    //prints the list to the console
    @Override
    public void printList() {
        if(size==1){
            System.out.println(first.getCurrent());
        } else if (size != 0) {
            MyNode<E> currentNode = first;
            System.out.println(currentNode.getCurrent());
            for (int i = 0; i < size; i++) {
                currentNode = currentNode.getNext();
                if (currentNode != null) {
                    System.out.println(currentNode.getCurrent());
                }
            }
        }


         else System.out.println("list is empty");
    }
// remove element by index and return element
    @Override
    public E remove(int index) {
        checkIndex(index);
        if(index==0){
            return removeFirst();

        } else if(index==size){

            return    removeLast();
        } else {
            MyNode<E> currentNode = first;
            for (int i = 0; i < index; i++) {
                currentNode = currentNode.getNext();
            }
            currentNode.getPrevious().setNext(currentNode.getNext());
            currentNode.getNext().setPrevious(currentNode.getPrevious());
            size--;
            return currentNode.getCurrent();
        }

    }

// return size of list
    @Override
    public int getSize() {
        return size;
    }
// Add element in first position
    @Override
    public void addFirts(E element) {
        if (size == 0) {
            initializeLink(element);
        } else if(size==1) {
                first.current = element;
                size++;
        } else {
            MyNode<E> second = first;
            first = new MyNode<>(element, second, null);
            second.setPrevious(first);
            size++;
        }

    }
//Add element in last position
    @Override
    public void addLast(E element) {
        if (size == 0) {
            initializeLink(element);
        } else if (size == 1) {
            last.current = element;
            size++;
        } else {
            MyNode<E> previous = last;
            last = new MyNode<E>(element, null, previous);
            previous.setNext(last);
            size++;
        }

    }
    //Remove element at first position and return this element
    @Override
    public E removeFirst() {
            checkSize();
            MyNode<E> currentNode = first;
            first = first.getNext();
            first.setPrevious(null);
            size--;
            return currentNode.getCurrent();
    }
    //Remove element at last position and return this element
    @Override
    public E removeLast() {
            checkSize();
            MyNode<E> currentNode = last;
            last = last.getPrevious();
            last.setNext(null);

            size--;
            return currentNode.getCurrent();

    }

    // Get element at first position
    @Override
    public E getFirst() {
        checkSize();
        return first.getCurrent();

    }

    // Get element at last position
    @Override
    public E getLast() {
       checkSize();
       return last.getCurrent();
    }
    //Create links in first and last elements
    private void initializeLink(E element){
        first.setCurrent(element);
        last.setCurrent(element);
        first.setNext(last);
        last.setPrevious(first);
        size++;
    }
    //check index
    private void checkIndex(int index) {
        if(index<0||index>size) throw new IllegalArgumentException();
    }
    //check size
    private void checkSize(){if (size<=0) throw new IllegalArgumentException(); }


}
