package ImplClass;
/*Реализовать Индексированный и Связной  списки

        Должна быть иерархия классов-интерфейсов
        Списки должны быть дженеризированы
        Списки могут содержать только объекты(не примитивные типы)
        Получение размера Списка - фактическое количество элементов в списке
        Возможность задавать начальный размер Списка при создании + размер по умолчанию

       Индексированный список
	Доступ к элементам Списка по индексу
	Вставка, удаление, изменение объекта в Списке по индексу
	(после удаления между элементами не должно быть пробелов)


        середина - имеется ввиду любое место между первым и последним элементом
        Использование сторонних библиотек и стандартных коллекций Java
        запрещено (искл. массивы)*/


import interfaces.MyList;


public class MyArrayList <E> implements MyList<E> {
    private int size;
    private E[] values;
    private final int DEFAULT_CAPACITY = 16;

    public MyArrayList(){
    values = (E[]) new Object[DEFAULT_CAPACITY];
    }

    public MyArrayList(int size) {
        if (size >= 0) {
            values = (E[]) new Object[size];
        }
        else throw new IllegalArgumentException();
    }


    public void update(int index, E element){
        checkIndex(index);
        values[index] = element;

    }


    //Get Element by index
    @Override
    public E get(int index) throws IllegalArgumentException {
        checkIndex(index);
        return values[index];
    }
    //Set element by index
    @Override
    public void set(int index, E element){
        checkIndex(index);
        values[index] = element;
    }
    //Add element in end of list. If list full - increase size
    @Override
    public boolean add(E element) {
        try {

            if (size == values.length-1) {

                E[] arr = values;
                values = (E[]) new Object[arr.length*2];
                System.arraycopy(arr, 0, values, 0, arr.length);

                values[size] = element;
                size++;
                return true;

            } else {
                values[size] = element;
                size++;
            }

            } catch(ClassCastException ex){
                ex.printStackTrace();
            }
            return false;
    }
        //delete element by index
    @Override
    public void delete(int index) {
        checkIndex(index);
        try{
            System.arraycopy(values,index+1, values,index, values.length-index-1);
            size--;
        } catch (ClassCastException ex){
            ex.printStackTrace();
        }
    }

    //prints the list to the console
    @Override
    public void printList() {

    for(int i=0;i<values.length;i++){
        if(values[i]!=null)
        System.out.println( values[i] + " " );
    }

    }
    // remove element by index and return element
    @Override
    public E remove(int index) {
        checkIndex(index);
        try{
            E element = get(index);
            System.arraycopy(values,index+1, values,index, values.length-index-1);
            size--;
            return element;
        } catch (ClassCastException ex){
            ex.printStackTrace();
        }
       throw new IllegalArgumentException();
    }
// get capacity
    public int capacity() {
        return values.length;
    }
// get size
    @Override
    public  int getSize(){
        return size;
    }
//check index
    private void checkIndex(int index){
    if(index<0 || index>=values.length-1) throw new IllegalArgumentException();
}

}
