package POJO;

public class Car{
    String model;
    int doors;

    public Car(String model, int doors) {
        this.doors = doors;
        this.model = model;
    }
    public Car(){}

    @Override
    public String toString() {
        return "Модель: " + model + " кол-во дверей: " + doors;
    }
}
