package interfaces;

public interface MyCollection<E> {
    int getSize();
    boolean add(E element);

}
