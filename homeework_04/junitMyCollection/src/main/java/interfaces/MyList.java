package interfaces;

public interface MyList<E> extends MyCollection<E>{



    E get(int index);

    void set(int index, E element);

    void delete(int index);

    void printList();

    E remove(int index);

    int getSize();


}
