import ImplClass.MyArrayList;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.internal.hamcrest.HamcrestArgumentMatcher;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Every.everyItem;
import java.util.ArrayList;

public class MyArrayListTest {
    MyArrayList<String> mylist;
    @Before
    public void setUp(){
        mylist = new MyArrayList<String>();
    }

    @Test
    public void sizeIsNotEqualToCapacity(){
        Assert.assertNotEquals(mylist.getSize(),mylist.capacity());
    }

    @Test
    public void addElementAndReturnSizeOne(){
        //Given
        int expectedSize=1;
        //When
        mylist.add("First element");
        //Then

        assertThat(mylist.getSize(),is(expectedSize));


    }
    @Test
    public void UpdateTheElementInTheFifthPositionAndReturnThisElement() {
        //Given
        String newFifthElement = "New Fifth Element";
        mylist.add("Zero");
        mylist.add("First");
        mylist.add("Second");
        mylist.add("Third");
        mylist.add("Fourth");
        mylist.add("Fifth");
        //When
        mylist.update(5, newFifthElement);
        //Then
        assertThat(mylist.get(5),is(newFifthElement));
    }


    @Test
    public void TryToAddMoreElementsThanInitialSize(){
        //Given
        int expected =21;
        //When
            for (int i = 0;i<=20;i++){
                mylist.add("String #" + i);
            }
        //Then
        assertThat(mylist.getSize(),is(expected));
    }

    @Test
    public void SetTheElementInTheFifthPositionAndReturnThisElement(){
    //Given
        String newFifthElement = "New Fifth Element";
        mylist.add("Zero");
        mylist.add("First");
        mylist.add("Second");
        mylist.add("Third");
        mylist.add("Fourth");
        mylist.add("Fifth");
    //When
        mylist.set(5,newFifthElement);
    //Then
        assertThat(mylist.get(5),is(newFifthElement));
    }

    @Test
    public void getElementInPositionZero(){
        //Given
        String ZeroElement = "Zero";
        //When
        mylist.add(ZeroElement);
        //Then
        assertThat(mylist.get(0),is(ZeroElement));
    }

    @Test
  public void deleteFifthElementAndReturnSizeFive(){
        //Given
        mylist.add("Zero");
        mylist.add("First");
        mylist.add("Second");
        mylist.add("Third");
        mylist.add("Fourth");
        mylist.add("Fifth");
        //When
        mylist.delete(5);
        //Then
        assertThat(mylist.getSize(),is(5));
    }
    @Test
    public void RemoveFifthElementAndReturnThisElement(){
        //Given
        mylist.add("Zero");
        mylist.add("First");
        mylist.add("Second");
        mylist.add("Third");
        mylist.add("Fourth");
        mylist.add("Fifth");
        String ElementBeforeRemove = mylist.get(5);
        //When
        String fifthElement = mylist.remove(5);
        //Then
        assertThat(ElementBeforeRemove,is(fifthElement));
    }

    @Test
    public void ShouldNotChangeTheSizeOfTheListAfterReplacement(){
       //Given
        mylist.add("Zero");
        mylist.add("First");
        mylist.add("Second");
        mylist.add("Third");
        mylist.add("Fourth");
        mylist.add("Fifth");
        int sizeBefore = mylist.getSize();
        //When
        mylist.update(1,"NewFirst");
        //Then
        assertThat(mylist.getSize(),is(sizeBefore));
    }


//Hamcrest
    @Test
    public void InnerVariableEqualsToGetSizeMethod(){
        assertThat(mylist,hasProperty("size",equalTo(mylist.getSize())));
    }




//Negative
    @Test(expected = IllegalArgumentException.class)
    public void tryToGetElementAtIncorrectPosition(){
        mylist.get(-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void tryToRemoveElementAtIncorrectPosition(){
        mylist.remove(-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void tryToSetElementAtIncorrentPosition(){
        mylist.set(25,"Element");
    }

    @Test(expected = IllegalArgumentException.class)
    public void tryToDeleteElementAtIncorrectPosition(){
        mylist.delete(251);
    }








}
