import ImplClass.MyLinkedList;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.BDDMockito;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;

public class MyLinkedListTest {
    MyLinkedList<String> list;
    @Rule public ExpectedException expectedException = ExpectedException.none();


    @Before
    public void setUp(){
    list = new MyLinkedList<>();
    }

    @Test
    public void addFirstElementAndReturnSizeOne(){
    //Given
        int expectedSize = 1;
    //When
        list.addFirts("first");
    //Then
        Assert.assertEquals(expectedSize,list.getSize());
    }

    @Test
    public void addFirstElementTwiceAndReturnCorrect(){
    //Given
        String First= "first";
        String Second = "second";
        //When
        list.addFirts(First);
        list.addFirts(Second);
        //Then
        Assert.assertEquals(Second,list.getFirst());
    }

    @Test
    public void addLastElementAndReturnSizeOne(){
        //Given
        int expectedSize = 1;
        //When
        list.addLast("last");
        //Then
        Assert.assertEquals(expectedSize,list.getSize());
    }
    @Test
    public void addLastElementTwiceReturnSizeTwoAndFirstAndLastElement(){
        //Given
        int expectedSize = 2;
        String last1 = "last#1";
        String last2 = "last#2";
        //When
        list.addLast(last1);
        list.addLast(last2);
        //Then
        Assert.assertEquals(expectedSize,list.getSize());
        Assert.assertEquals(last1,list.getFirst());
        Assert.assertEquals(last2,list.getLast());
    }

    @Test
    public void addElementAtIndexFiveAndReturnExElementFiveAfterOffset(){
        //Given
        String numberFive =  "the same element";
            for(int i = 0;i<10;i++){
                if(i==5) {
                    list.add(numberFive);
                } else list.add("Element #"+i);
            }
        //When
            list.add(5,"NewFive");
         //Then
        Assert.assertEquals(numberFive,list.get(6));
    }
    @Test
    public void addElementAtZeroPositionFirstElementMustBeEqualsLast(){
        list.add(0,"Element");
        Assert.assertEquals(list.getFirst(),list.getLast());
    }

    @Test
    public void RemoveFirstElementAndReturn(){
        //Given
        String expectedElement = "expected element";
        list.add(expectedElement);
        for(int i = 0;i<10;i++){
            list.add("Element #"+i);
        }
        //When
        String removedElement = list.removeFirst();
        //Then
        Assert.assertEquals(expectedElement,removedElement);
    }

    @Test
    public void RemoveLastElementAndReturn(){
        //Given
        String expectedElement = "expected element";
        for(int i = 0;i<10;i++){
         list.add("Element #"+i);
        }
        list.add(expectedElement);
        //When
        String removedElement = list.removeLast();
        //Then
        Assert.assertEquals(expectedElement,removedElement);
    }
    @Test
    public void getElementByIndexFiveMustBeEqualBatman(){
        //Given
        String expectedElement = "Batman";
        list.add(0,"Spider-man");
        list.add(1,"Wolverine");
        list.add(2,"Cat-woman");
        list.add(3,"Doctor Strange");
        list.add(4,"Spider-woman");
        list.add(5,"Batman");
        //When
        String result = list.get(5);
        //Then
        Assert.assertEquals("Batman",list.get(5));
    }
    @Test
    public void RemoveElementByIndexFourElementMustReturnValueEqualBatmanAndSizeMustBeEqualFive(){
        //Given
        list.add(0,"Spider-man");
        list.add(1,"Wolverine");
        list.add(2,"Cat-woman");
        list.add(3,"Doctor Strange");
        list.add(4,"Batman");
        list.add(5,"Spider-woman");
        //When
        String result = list.remove(4);
        //Then
        Assert.assertEquals("Batman",result);
        Assert.assertEquals(5,list.getSize());
    }
    @Test
    public void FirstElementMustBeEqualSpiderManAndLastElementMustBeEqualSpiderWoman(){
        //Given
        list.add(0,"Spider-man");
        list.add(1,"Wolverine");
        list.add(2,"Cat-woman");
        list.add(3,"Doctor Strange");
        list.add(4,"Batman");
        list.add(5,"Spider-woman");
        //When
        String expectedFirst = list.getFirst();
        String expectedLast = list.getLast();
        //Then
        Assert.assertEquals("Spider-man",expectedFirst);
        Assert.assertEquals("Spider-woman",expectedLast);
    }
    @Test
    public void DeleteElementByIndexFiveSizeMustBeEqualNine(){
        //Given
        for(int i = 0;i<10;i++){
            list.add("Element #"+i);
            }
        //When
        list.delete(5);
        //Then
        Assert.assertEquals(9,list.getSize());
    }
    @Test
    public void SetElementByIndexMustBeEqualToTheReturnedElement(){
        //Given
        String expectedElement = "expected element";
        for(int i = 0;i<10;i++){
            list.add("Element #"+i);
        }
        //When
        list.set(4,expectedElement);
        //Then
        Assert.assertEquals(expectedElement,list.get(4));
    }

    //Hamcrest
    @Test
    public void InnerVariableEqualsToGetSizeMethod(){
        assertThat(list,hasProperty("size",equalTo(list.getSize())));
    }


    //Negative
    @Test
    public void tryToRemoveFirstElementFromEmptyList() {
        expectedException.expect(IllegalArgumentException.class);
        list.removeFirst();
    }
    @Test
    public void tryToRemoveLastElementFromEmptyList() {
        expectedException.expect(IllegalArgumentException.class);
        list.removeFirst();
    }
    @Test
    public void tryToDeleteElementFromEmptyList(){

        expectedException.expect(IllegalArgumentException.class);
        list.delete(2);
    }
    @Test
    public void tryToSetElementAtIncorrectPosition() {

        expectedException.expect(IllegalArgumentException.class);
        list.set(-1,"element");
    }
    @Test
    public void tryToRemoveElementAtIncorrectPosition() {
        expectedException.expect(IllegalArgumentException.class);
        list.set(22,"element");
    }
    @Test
    public void tryToGetElementFromEmptyList() {
        expectedException.expect(IllegalArgumentException.class);
        list.get(2);
    }
    @Test
    public void tryToGetFirstElementFromEmptyList() {
        expectedException.expect(IllegalArgumentException.class);
        list.getFirst();
    }
    @Test
    public void tryToRemoveAndGetFirstElementFromListWithOneElement(){
        expectedException.expect(IllegalArgumentException.class);
        list.addLast("element");
        list.removeLast();
        list.getLast();
    }


}
