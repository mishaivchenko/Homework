import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verifyNoMoreInteractions;

public class ProcessorMockTest {
    @Mock Consumer consumerTest;
    @Mock Producer producerTest;
    @Captor ArgumentCaptor<String> argumentCaptor ;
    @InjectMocks Processor processorTest;
    @Rule public ExpectedException expectedException = ExpectedException.none();


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void producerAnswerTestTheProduceMethodMustBeCalledOnce(){
        //Given
        Mockito.when(producerTest.produce()).thenCallRealMethod();
        //When
         processorTest.process();
        //Then
        Mockito.verify(producerTest, times(1)).produce();
        Mockito.verifyNoMoreInteractions(producerTest);
    }

    @Test
    public void consumerAnswerTestTheConsumeMethodMustBeCalledOnce(){
        //Given
        Mockito.when(producerTest.produce()).thenReturn("value");
        //When
        processorTest.process();
        //Then
        verify(consumerTest,times(1)).consume("value");
        verifyNoMoreInteractions(consumerTest);
    }

    @Test
    public void ConsumerArgumentTestArgumentValueMustReturnStringEqualsExpectedString(){
    //Given
        String expectedValue = "expectedValue";
        argumentCaptor = ArgumentCaptor.forClass(String.class);
        Mockito.when(producerTest.produce()).thenReturn(expectedValue);
    //When
        processorTest.process();
    //Then
           verify(consumerTest).consume(argumentCaptor.capture());
            Assert.assertEquals(expectedValue,argumentCaptor.getValue());
    }

    @Test
    public void ProducerThrowIllegalStateException(){
        expectedException.expect(IllegalStateException.class);
        Mockito.when(producerTest.produce()).thenReturn(null);
        processorTest.process();
    }
}
