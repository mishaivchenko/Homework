package firstTask;/*1. Using Stream API write a method that returns a comma separated string based on a given list of integers.
        Each element should be preceded by the letter 'e' if the number is even, and preceded by the letter 'o' if the number is odd.
        For example, if the input list is (3,44), the output should be 'o3,e44'.*/


import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Separated {
    public static void main(String[] args) {

   String out =  Arrays.asList(3,2,5,6,7,8,9,10,11).stream().map(x->x%2==0 ? "e"+x:"o"+x).reduce(( a,  b)->a.concat("," + b)).get();

   System.out.println(out);

    }
}
