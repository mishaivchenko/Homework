package thirdTask;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/*
3. Write a method public static <T> Stream<T> thirdTask.zip(Stream<T> first, Stream<T> second)
        that alternates elements from the streams first and second, stopping when
        one of them runs out of elements
*/
public class Zipper {

    public static void main(String[] args) {
        Zipper zipper = new Zipper();
        Stream first = Arrays.asList(3, 2, 5, 6, 7, 8, 9, 10, 11).stream();
        Stream second = Arrays.asList(72, 45, 26, 17, 98, 89, 101, 1221).stream();
        Stream result = zipper.zip(first, second);
        result.forEach(System.out::println);
    }


    public  <T> Stream<T> zip(Stream<T> first, Stream<T> second) {
        if (first != null && second != null) {
            List<T> listFirst = first.collect(Collectors.toList());
            List<T> listSecond = second.collect(Collectors.toList());

             Stream.Builder<T> streamBuilder = Stream.builder();

                    IntStream.range(0, Math.min(listFirst.size(), listSecond.size()))
                    .forEach(i -> {
                        streamBuilder.add(listFirst.get(i));
                        streamBuilder.add(listSecond.get(i));
                    });

            return streamBuilder.build();
        } else throw new NullPointerException();
    }
}