package secondTask;

public class City{
    private String state;
    private long population;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public long getPopulation() {
        return population;
    }

    public void setPopulation(long population) {
        this.population = population;
    }

    public City(String state, long population) {
        this.state = state;
        this.population = population;
    }

    @Override
    public String toString() {
        return "City{" +
                "population=" + population +
                '}';
    }
}