package secondTask;
/*
2. Using Stream API implement a method that produces the largest city per state
        Map<String, City> getLargestCityPerState(Collection<City> cities){

        }

class City{
    private String state;
    private long population;
}
*/

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class LargestCity {

    public static void main(String[] args) {
        LargestCity largestCity = new LargestCity();
        Collection<City> cities = new ArrayList<City>();

        ((ArrayList<City>) cities).add(new City("alaska", 10000));
        ((ArrayList<City>) cities).add(new City("alaska", 20000));
        ((ArrayList<City>) cities).add(new City("alaska", 70000));
        ((ArrayList<City>) cities).add(new City("alaska", 40000));
        ((ArrayList<City>) cities).add(new City("alaska", 50000));

        ((ArrayList<City>) cities).add(new City("Nevada", 30000));
        ((ArrayList<City>) cities).add(new City("Nevada", 40000));
        ((ArrayList<City>) cities).add(new City("Nevada", 50000));
        ((ArrayList<City>) cities).add(new City("Nevada", 30000));
        ((ArrayList<City>) cities).add(new City("Nevada", -10000));

        Map LargestCities = largestCity.getLargestCityPerState(cities);
        LargestCities.forEach((k, v) -> System.out.println("key: " + k + " value:" + v));

    }

    public Map<String, City> getLargestCityPerState(Collection<City> cities) {

        if (cities != null) {
            Map<String, City> result = new HashMap<>();

            cities.stream().forEach(i -> {
                if (!result.containsKey(i.getState())) {
                    result.put(i.getState(), i);
                } else if (result.get(i.getState()).getPopulation() <= i.getPopulation()) {
                    result.put(i.getState(), i);
                }
            });
            return result;
        } else throw new NullPointerException();
    }
}
