package fourthTaskWithObserver;

import fourthTaskWithObserver.interfaces.Observed;
import fourthTaskWithObserver.interfaces.Observer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class actionWithTheBooks implements Observed {


    List<Book> Books = new ArrayList<>();
    List<Observer> subsctibers = new ArrayList<>();
    Map<String,Integer> map = new HashMap();

    public void addBook(Book book){
        this.Books.add(book);
        changeMapAdd(book);

    }
    public void removeBook(Book book){
        this.Books.remove(book);
        changeMapRemove(book);
        notifyObs();
    }

    public void changeMapAdd(Book book){
        if (!map.containsKey(book.getAuthor())){
            map.put(book.getAuthor(),1);
        } else {
            int i = map.get(book.getAuthor());
            map.remove(book.getAuthor());
            map.put(book.getAuthor(),i+1);
        }
        notifyObs();
    }


    public void changeMapRemove(Book book){
        if(map.containsKey(book.getAuthor())&&(map.get(book.getAuthor())>0)){
            int i = map.get(book.getAuthor());
            map.put(book.getAuthor(),i-1);
        }
    }


    @Override
    public void addObserver(Observer observer) {
        this.subsctibers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        this.subsctibers.remove(observer);
    }

    @Override
    public void notifyObs() {
        for(Observer observer:subsctibers){
            observer.handleEvent(this.Books,this.map);

        }

    }
}
