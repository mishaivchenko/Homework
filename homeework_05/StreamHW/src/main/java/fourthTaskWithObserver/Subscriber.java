package fourthTaskWithObserver;

import fourthTaskWithObserver.interfaces.Observer;

import java.util.List;
import java.util.Map;

public class Subscriber implements Observer {

    @Override
    public void handleEvent(List<Book> Books, Map<String,Integer> authors) {
        System.out.println("The List was changed");
        System.out.println(authors);
            
        }
        
    }

