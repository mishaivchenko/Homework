package fourthTaskWithObserver;

public class BooksAuthorMain {
    Book book;

    public static void main(String[] args) {
        actionWithTheBooks actionWithTheBooks = new actionWithTheBooks();
        actionWithTheBooks.addBook(new Book("Книга джунглей","Киплинг","isbn:5875-51351-4687-1234"));
        actionWithTheBooks.addBook(new Book("Тарас Бульба","Гоголь","isbn:4275-5112-4627-1234"));
        actionWithTheBooks.addBook(new Book("Всадник без головы","Майн Рид","isbn:3275-1112351-4687-1234"));
        actionWithTheBooks.addBook(new Book("Стальная крыса","Гаррисон","isbn:275-1351-4287-4234"));
        actionWithTheBooks.addBook(new Book("Философия java","Эккель","isbn:1871-2192-4237-5134"));
        actionWithTheBooks.addBook(new Book("Хижина","Янг","isbn:5215-53251-4287-1134"));

        Subscriber subscriber = new Subscriber();
        actionWithTheBooks.addObserver(subscriber);
        Book book = new Book("Valentin","Valyaaaa","pdijadjaw");
        actionWithTheBooks.addBook(new Book("Борода","Янг","isbn:5215-551-4287-1134"));

        actionWithTheBooks.addBook(book);
        actionWithTheBooks.removeBook(book);
    }


}
