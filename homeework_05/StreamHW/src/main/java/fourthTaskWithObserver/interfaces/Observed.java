package fourthTaskWithObserver.interfaces;

public interface Observed {
     void addObserver(Observer observer);

     void removeObserver(Observer observer);


    void notifyObs();
}
