package fourthTaskWithObserver.interfaces;

import fourthTaskWithObserver.Book;

import java.util.List;
import java.util.Map;

public interface Observer {
    void handleEvent(List<Book> Books, Map<String,Integer> authors);
}
