import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import thirdTask.Zipper;

import java.lang.reflect.Array;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class thirdTask {
    Zipper zipper;


    @Rule public ExpectedException expectedException = ExpectedException.none();
    @Before
    public void setUp() {
        zipper = new Zipper();
    }

    @Test
    public void tryToAlternatesIntResultLenghtMustBeEqual_16() {
        //Given

        Stream first = Arrays.asList(3, 2, 5, 6, 7, 8, 9, 10, 11).stream();
        Stream second = Arrays.asList(72, 45, 26, 17, 98, 89, 101, 1221).stream();
               //When
        Stream result = zipper.zip(first, second);
        //Then
        System.out.println();
        Assert.assertEquals(result.count(),16);
            }
    @Test
    public void sequenceCheck() {
        //Given
        String[] mas = {"Vasya", "Inokentiy", "Petya", "Valerian", "Masha", "Dimitriy", "Misha", "Maskim", "Kolya", "Evgen", "Taras", "Viola"};
        Stream first = Arrays.asList("Vasya", "Petya", "Masha", "Misha", "Kolya", "Taras").stream();
        Stream second = Arrays.asList("Inokentiy","Valerian", "Dimitriy", "Maskim", "Evgen", "Viola", "Mufusail", "Petro").stream();

        //When
        Stream result = zipper.zip(first, second);
        List list1 = (List) result.collect(Collectors.toList());
        //Then
        for (int i = 0;i<list1.size();i++){
            Assert.assertEquals(true,list1.get(i)==mas[i]);
        }
    }

    @Test
    public void tryToPutNullInMethodArgs(){
        expectedException.expect(NullPointerException.class);
        Stream result = zipper.zip(null,null);
    }




}


