import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import secondTask.LargestCity;
import secondTask.City;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

public class secondTask {
    LargestCity largestCity;
    City city;
@Rule public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp(){
    largestCity = new LargestCity();
    }

    @Test
    public void addThreeValueWithSameStateAndDifferentPopulationLargestMustBeSecond(){
        //Given
        Collection<City> cities = new ArrayList<City>();
      City first = new City("Alaska",4657);
      City second = new City("Alaska",197465);
      City third = new City("Alaska",100000);
        ((ArrayList<City>) cities).add(first);
        ((ArrayList<City>) cities).add(second);
        ((ArrayList<City>) cities).add(third);
        //When
        Map map = largestCity.getLargestCityPerState(cities);
        //Then
        Assert.assertEquals(second,map.get("Alaska"));
    }


    @Test
    public void tryToPutNullInMethodArgs(){
        expectedException.expect(NullPointerException.class);
        largestCity.getLargestCityPerState(null);
    }


}
