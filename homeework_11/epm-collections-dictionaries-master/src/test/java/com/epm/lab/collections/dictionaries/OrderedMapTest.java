package com.epm.lab.collections.dictionaries;

import com.epm.lab.collections.Map;
import com.epm.lab.collections.dictionaries.orderedMap.MyBinaryTreeMap;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
public class OrderedMapTest {


    @Test
    public void sizeTestAfterTestSizeMustBeInc(){
       //Given
        int expectedSize = 2;
        Map<String,String> map = new MyBinaryTreeMap<>();
        //When
        map.put("one","one");
        map.put("two","two");
        //Then
        assertEquals(expectedSize,map.size());
    }

    @Test
    public void putTestAfterTestMapMustHaveExpectedValue(){
        //Given
        String expectedString = "expectedString";
        Map<String,String> map = new MyBinaryTreeMap<>();
        //When
        map.put("one","one");
        map.put("two","two");
        map.put(expectedString,expectedString);
        //Then
        assertEquals(expectedString,map.get(expectedString));
    }
    @Test
    public void duplicateTestAfterTestSizeMustBeNotChanged(){
        //Given
        int expectedSize = 2;
        Map<String,String> map = new MyBinaryTreeMap<>();
        //When
        map.put("one","one");
        map.put("two","two");
        map.put("one","one");
        //Then
        assertEquals(expectedSize,map.size());
    }
    @Test
    public void putValueWithSameKeyButAnotherValueTestAfterTestValueMustBeChanged(){
        //Given
        Map<String,String> map = new MyBinaryTreeMap<>();
        String expectedValue = "two";
        //When
        map.put("one","one");
        map.put("two","two");
        map.put("one","two");
        //Then
        assertEquals(expectedValue,map.get("one"));
    }


}
