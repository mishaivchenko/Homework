package com.epm.lab.collections.dictionaries.orderedMap.myStack;

public interface MyStack<Node> {
    void push(Node node);
    Node peek();
    Node pop();
}
