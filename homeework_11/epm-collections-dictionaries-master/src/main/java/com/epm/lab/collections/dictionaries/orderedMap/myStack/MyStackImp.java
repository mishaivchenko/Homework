package com.epm.lab.collections.dictionaries.orderedMap.myStack;

public class MyStackImp<K> implements MyStack<K> {

private stackNode<K> top = null;


   private static class stackNode<E>{
       private E value;
       private stackNode<E> next;
            private stackNode(E value, stackNode<E> next){
                this.value= value;
                this.next = next;
            }
   }


    @Override
    public void push(K k) {
        this.top = new stackNode<>(k,this.top);
    }

    @Override
    public K peek() {
        stackNode<K> stackNode = this.top;
        if(stackNode!=null) {
            return stackNode.value;
        } else return null;
        }


    @Override
    public K pop() {
        stackNode<K> oldNode = this.top;
        this.top = this.top.next;
        return oldNode.value;
    }
}
