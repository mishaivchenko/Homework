package com.epm.lab.collections.dictionaries.iterator;

import com.epm.lab.collections.dictionaries.orderedMap.MyBinaryTreeMap;
import com.epm.lab.collections.dictionaries.orderedMap.myStack.MyStack;
import com.epm.lab.collections.dictionaries.orderedMap.myStack.MyStackImp;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class TreeIterator implements Iterator {
    MyBinaryTreeMap.Node root;
    MyStack<MyBinaryTreeMap.Node> myStack = new MyStackImp<>();

    public TreeIterator(MyBinaryTreeMap.Node root) {
    this.root = root;
    pushLeftChild(root);
    }


    private void pushLeftChild(MyBinaryTreeMap.Node root) {
        while (root!=null) {
            myStack.push(root);
            root = root.getLeftChildNode();
        }
    }
    @Override
    public boolean hasNext() {
        if (myStack.peek() == null) {
            return false;
        } else return true;
    }

    @Override
    public MyBinaryTreeMap.Node next() {
        if(!hasNext()) throw new NoSuchElementException();
        MyBinaryTreeMap.Node node = myStack.pop();
        pushLeftChild(node.getRightChilNode());
        if(node!=null) return node;
        else return null;
    }

}


