package com.epm.lab.collections.dictionaries;

import com.epm.lab.collections.Map;
import com.epm.lab.collections.dictionaries.hashMap.MyHashMap;
import com.epm.lab.collections.dictionaries.iterator.TreeIterator;
import com.epm.lab.collections.dictionaries.orderedMap.MyBinaryTreeMap;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

import settings.Settings;

import java.io.*;
import java.nio.charset.StandardCharsets;

import java.util.Iterator;

public class DemoMyHashMapMyBinaryTreeMap {


    public static void main(String[] args) throws IOException {


        Settings settings = new Settings();
        CmdLineParser cmdLineParser = new CmdLineParser(settings);

        try {
            cmdLineParser.parseArgument(args);
            Map<String, Integer> hashMap = new MyHashMap<>();
            Map<String, Integer> binaryTreeMap = new MyBinaryTreeMap<>();
            File file = settings.getBookFile();
            try (BufferedReader reader = new BufferedReader(
                    new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8))) {
                String line;
                while ((line = reader.readLine()) != null) {
                    String[] splitResult = line.split("[,\\. �;:]");
                    for (String s : splitResult) {
                        hashMap.put(s, 0);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println(hashMap.size());
            Iterator<Map.Entry<String, Integer>> iterator = hashMap.iterator();
            while (iterator.hasNext()) {
                binaryTreeMap.put(iterator.next().key, 0);
            }
            TreeIterator iteratorForTree = (TreeIterator) binaryTreeMap.iterator();
            while (iteratorForTree.hasNext()) {
                System.out.println(iteratorForTree.next().getCurrentEntry().key);
            }
        } catch (CmdLineException e) {
            cmdLineParser.printUsage(System.out);
        }

    }
}


