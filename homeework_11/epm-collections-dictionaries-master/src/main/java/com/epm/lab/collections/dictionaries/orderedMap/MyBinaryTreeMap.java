package com.epm.lab.collections.dictionaries.orderedMap;

import com.epm.lab.collections.OrderedMap;
import com.epm.lab.collections.dictionaries.iterator.TreeIterator;

import java.util.Iterator;

public class MyBinaryTreeMap<K extends Comparable<K>,V> implements OrderedMap<K,V>{
    private Node<K,V> head;
    private int size;

    public class Node<K,V> {
        Entry<K,V> currentEntry;
        private  Node<K,V> leftChildNode;
        private Node<K,V> rightChilNode;

        Node(Entry<K,V> entry){
            this.currentEntry = entry;
        }

        public Entry<K, V> getCurrentEntry() {
            return currentEntry;
        }

        public void setCurrentEntry(Entry<K, V> currentEntry) {
            this.currentEntry = currentEntry;
        }

        public Node<K, V> getLeftChildNode() {
            return leftChildNode;
        }

        public void setLeftChildNode(Node<K, V> leftChildNode) {
            this.leftChildNode = leftChildNode;
        }

        public Node<K, V> getRightChilNode() {
            return rightChilNode;
        }

        public void setRightChilNode(Node<K, V> rightChilNode) {
            this.rightChilNode = rightChilNode;
        }
    }


    @Override
    public void put(K k, V v) {

            if (head == null) {
                head = new Node<>(new Entry<>(k, v));
                size++;
            } else{
                Node<K,V> node = contains(k);

                     if(node==null){
                         addChild(head, new Node<K,V>( new Entry<>(k, v)));
                        size++;
                        } else if(node!=null&&node.currentEntry.value!=v){
                         node.currentEntry.value = v;
                     }
            }
    }
    private void addChild(Node<K,V> node, Node<K,V> childNode){
        if(childNode.currentEntry.key.compareTo(node.currentEntry.key)<0){
            if(node.leftChildNode==null){
                node.leftChildNode = childNode;

            } else {
                addChild(node.leftChildNode, childNode);
            }
        } else if(childNode.currentEntry.key.compareTo(node.currentEntry.key)>0){
            if(node.rightChilNode==null){

                node.rightChilNode = childNode;

            } else {
                addChild(node.rightChilNode, childNode);
            }
        }
    }

    @Override
    public V get(K k) {
        Node<K, V> node = contains(k);
        if (node != null) {
            return node.currentEntry.value;
        } else return null;
    }

    public Node<K,V> contains(K key){
        Node<K, V> parentNode = null;
        Node<K, V> currentNode = head;

        while (currentNode != null) {
            int compareResult = currentNode.currentEntry.key.compareTo(key);
            if (compareResult > 0) {
                parentNode = currentNode;
                currentNode = currentNode.leftChildNode;
            } else if (compareResult < 0) {
                parentNode = currentNode;
                currentNode = currentNode.rightChilNode;
            } else {
                break;

            }
        }
        if (currentNode != null) {
            return currentNode;
        } else return null;
    }
    @Override
    public int size() {
        return size;
    }

    @Override
    public Iterator<Entry<K, V>> iterator() {
    return new TreeIterator(this.head);
    }



}
