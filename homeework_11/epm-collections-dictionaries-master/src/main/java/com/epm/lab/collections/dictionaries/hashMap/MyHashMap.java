package com.epm.lab.collections.dictionaries.hashMap;

import com.epm.lab.collections.Map;

import java.util.Iterator;


public class MyHashMap<K,V> implements Map<K,V> {
    private int size = 0;
    private float threshold;
    private Bucket[] buckets;
    private Entry<K, V> entry;

    @Override
    public int hashCode() {
        int hash = 31;
        return hash * 17 + entry.hashCode();
    }

    private class Bucket<E> {
        Entry<K, V>[] table;
        int pointer;

        Bucket() {
            table = new Entry[100];
            this.pointer = 0;
        }
    }

    public MyHashMap() {
        buckets = new Bucket[16];
        threshold = buckets.length * 0.75f;
    }

    @Override
    public V get(K key) {
        int index = createIndex(key);
        for (Entry<K,V> entry : buckets[index].table) {
            if (entry.key.equals(key)) return entry.value;
        };
        return null;
    }

    private void doubleTheArray() {
        Bucket[] oldBuckets = buckets;
        buckets = new Bucket[oldBuckets.length * 2];
        size = 0;
        for (Bucket bucket : oldBuckets) {

            if (bucket != null) {
                for (Entry<K, V> entry : bucket.table) {
                    if (entry != null) put(entry.key, entry.value);
                }
            }
        }
    }

    private int createIndex(K key) {
        int hash = key.hashCode();
        if (hash > 0) {
            return hash % buckets.length;
        } else return (hash * (-1)) % buckets.length;
    }

    @Override
    public void put(K key, V value) {
        int index;
        if (size + 1 >= threshold) {
            threshold = threshold * 2;
            doubleTheArray();
        }
        entry = new Entry<>(key, value);

        index = createIndex(key);

        if (buckets[index] == null) {
            buckets[index] = new Bucket();
            buckets[index].table[0] = entry;
            buckets[index].pointer++;
            size++;
        } else {
            if (buckets[index] != null) {
                if (!checkCheck(buckets[index].table, entry)) {
                    buckets[index].table[buckets[index].pointer] = entry;
                    buckets[index].pointer++;
                    size++;
                }
            }
        }
    }
    private boolean checkCheck(Entry[] mas, Entry newEntry) {
        for (Entry oldEntry : mas) {
            if (oldEntry != null) {
                if ((newEntry.key.equals(oldEntry.key)) && (!newEntry.value.equals(oldEntry.value))) {
                    oldEntry.value = newEntry.value;
                    return true;
                } else if ((newEntry.key.equals(oldEntry.key)) && (newEntry.value.equals(oldEntry.value))) return true;
            }
        }
        return false;
    }


    @Override
    public int size() {
        return size;
    }


    public void print() {
        for (Bucket bucket : buckets) {
            if (bucket != null) {
                System.out.println("BUCKKKKKETTTT");
                for (Entry<K, V> entry : bucket.table) {
                    if (entry != null)
                        System.out.println(entry.key + "  <---- entry key  entry value---->  " + entry.value);
                }
            }
        }
    }
    @Override
    public Iterator<Entry<K, V>> iterator() {
        return new Iterator<Entry<K, V>>() {
            int counterBuckets = 0;
            int valuesCounter = 0;
            Entry<K,V>[] entries;


            @Override
            public boolean hasNext() {
                if (counterBuckets == buckets.length) {
                    return false;
                }
                if (buckets[0] != null) {
                    entries = buckets[counterBuckets].table;
                }
                    if ((entries == null) || (entries[valuesCounter] == null)) {

                        if (buckets[counterBuckets].table[valuesCounter] == null) {
                            valuesCounter = 0;
                        }
                        if (goNext()) {
                            entries = buckets[counterBuckets].table;
                        } else return false;
                    }
                    return entries[valuesCounter] != null;

            }

            @Override
            public Entry<K,V> next() {
                Entry<K,V> entry = entries[valuesCounter++];

                return entry;
            }

            public boolean goNext(){
                counterBuckets++;
            while (counterBuckets<buckets.length&&buckets[counterBuckets]==null){
                counterBuckets++;
            }
            return counterBuckets<buckets.length&&buckets[counterBuckets]!=null;
            }
        };
    }

}