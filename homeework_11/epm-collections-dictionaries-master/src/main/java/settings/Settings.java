package settings;

import org.kohsuke.args4j.Option;

import java.io.File;

public class Settings {
    @Option(name = "-d", required = true, usage = "bookFile")
    private File bookFile;

    public File getBookFile() {
        return bookFile;
    }
}
